package imp.compose

import androidx.compose.material.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle


@Composable inline fun TextStyle.Text(
	text: String,
	modifier: Modifier = Modifier,
	color: Color = Color.Unspecified,
) = ProvideTextStyle(this) { androidx.compose.material.Text(
	text = text,
	modifier = modifier,
	color = color,
)}