package imp.org.android.ui.view

import android.content.Context
import androidx.compose.runtime.Composable
import imp.compose.view.CVHelper
import imp.org.android.LabelI
import imp.org.android.LabelRepo

/**Loads all labels and provides them to your block.*/
@Composable inline fun CVAllLabels(
	ctx: Context,
	block: @Composable (List<LabelI>?) -> Unit,
) = CVHelper(defaultLoad = { LabelRepo.getAll(ctx)}) { labels, _, _, _ ->
	block(labels)
}