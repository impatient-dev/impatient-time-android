package imp.org.android.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import imp.compose.CColMax
import imp.compose.CColWC
import imp.compose.CHL
import imp.compose.CRowWC
import imp.compose.CSpinnerHC
import imp.compose.CSurfaceFull
import imp.compose.CWeight
import imp.org.android.time.PastTimePeriodI
import imp.org.android.ui.AppTheme
import imp.util.fmt.TimeLetterFmt
import imp.util.toLocal
import java.time.Instant

/**Shows a list of past time periods in a vertical list, most recent first.*/
object RecentTimePeriodsScreen {
	interface Handler {
		fun loadMore()
		fun onSelect(period: PastTimePeriodI)
	}

	@Composable fun Compose(
		/**Most recent first; null if not loaded.*/
		periods: List<PastTimePeriodI>?,
		/**True if there are no more periods available.*/
		endOfList: Boolean,
		theme: AppTheme,
		handler: Handler?,
		now: Instant,
	) = CSurfaceFull {
		CColMax {
			if(periods == null)
				CSpinnerHC()
			else if(periods.isEmpty())
				theme.bodyWarn.C("No time periods stored")
			else
				NonEmptyList(periods, endOfList, theme, handler, now)
		}
	}


	@Composable fun NonEmptyList(
		periods: List<PastTimePeriodI>,
		endOfList: Boolean,
		theme: AppTheme,
		handler: Handler?,
		now: Instant,
	) = LazyColumn(horizontalAlignment = Alignment.CenterHorizontally) {
		items(periods.size) { i ->
			val period = periods[i]
			CColWC(Modifier.clickable { handler?.onSelect(period) }) {
				// end time
				CHL {
					val str = if(period.end == null) "now" else theme.todFmt.dateHm(period.end.toLocal())
					theme.body.C(str)
				}

				// labels or type
				if(period.uuid == null)
					theme.strongWarn.C("Untracked")
				else if(period.labels.isEmpty())
					theme.strongWarn.C("Unlabeled")
				else
					for(label in period.labels)
						theme.strong.C(label.name)

				// duration, and start time if it doesn't match the end time for the next period in the list (previous period in time)
				CRowWC {
					val end = period.end ?: now
					val ms = end.toEpochMilli() - period.start.toEpochMilli()
					if(i == periods.lastIndex || periods[i + 1].end != period.start)
						theme.body.C(theme.todFmt.dateHm(period.start.toLocal()))
					CWeight()
					theme.body.C(TimeLetterFmt.duration2(ms))
				}
			}
		}

		// afterwards: end-of-list
		item {
			if(endOfList)
				theme.strong.C("No further data")
			else
				theme.btn.C("Load More") { handler?.loadMore() }
		}
	}
}