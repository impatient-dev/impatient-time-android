package imp.org.android.ui.widgets

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import imp.compose.CColC
import imp.compose.CSurface
import imp.compose.maybeClickable
import imp.org.android.LabelI
import imp.org.android.time.CurrentTimePeriodI
import imp.org.android.todFmt
import imp.org.android.ui.AppTheme
import imp.org.android.ui.CAppTheme2
import imp.util.fmt.Millis
import imp.util.fmt.TimeFmt
import imp.util.hashedUuid
import java.time.Instant
import java.time.ZoneId
import java.util.UUID.randomUUID


object CCurrentTimePeriod {

	@Composable fun Compose(
		current: CurrentTimePeriodI,
		theme: AppTheme,
		onClick: () -> Unit = {},
		zone: ZoneId = ZoneId.systemDefault(),
		now: Instant = Instant.now(),
	) = CColC(Modifier.maybeClickable(onClick)) {
		theme.body.C("current activity")

		if(current.uuid == null) {
			theme.strongDisabled.C("nothing")
		} else if(current.labels.isEmpty()) {
			theme.strongWarn.C("unlabeled")
		} else {
			current.labels.forEach { label ->
				theme.strong.C(label.name)
			}
		}

		if(current.start == null) {
			theme.bodyWarn.C("since forever")
		} else {
			val sinceStr = remember(current.start, zone) {
				TimeFmt.pastApprox(current.start.toEpochMilli(), todFmt, zone = zone, nowMs = now.toEpochMilli())
			}
			theme.body.C("since $sinceStr")
		}
	}
}


@Preview @Composable private fun preview_0() = preview(null, null)

@Preview @Composable private fun preview_1() = preview(listOf("working on imp-org-android"), 5 * Millis.MINUTE)

@Preview @Composable private fun preview_2() = preview(listOf("biking", "outdoors"), 1 * Millis.HOUR + 15 * Millis.MINUTE)


@Composable private fun preview(
	/**Null if untracked.*/
	labels: List<String>?,
	forMs: Long?,
	now: Instant = Instant.now(),
) {
	val labelInfos = labels?.map { LabelI(id = -1, uuid = it.hashedUuid(), name = it, description = null, flags = 0, implies = emptySet(), impliedBy = emptySet()) } ?: emptyList()
	val start = if(forMs == null) null else now.minusMillis(forMs)
	val current = CurrentTimePeriodI(id = -1, uuid = randomUUID(), start = start, labels = labelInfos)
	CAppTheme2 { theme ->
		CSurface {
			CCurrentTimePeriod.Compose(current = current, theme = theme)
		}
	}
}