package imp.org.android.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import imp.compose.opaque
import imp.compose.opaquePair
import imp.compose.under
import imp.org.android.todFmt


private val cPrimary = opaque(0x37474f) // blue-gray 800
private val cSecondary = opaque(0xB0BEC5) // blue-gray 200

private val disabledLite = opaquePair(0xaaaaaa, 0x666666)


private val lightTheme = AppTheme(
	main = Color.White.under(),
	disabled = disabledLite,
	primary = cPrimary,
	primaryStrong = opaque(0x263238), // blue-gray 900
	foreBad = opaque(0xBF360C), // deep orange 900
	foreWarn = opaque(0xFF6F00), // amber 900
	foreGood = opaque(0x2E7D32), // green 800
	foreWeak = opaque(0x666666),
	todFmt = todFmt,
)


private val darkTheme = AppTheme(
	main = Color.Black.under(),
	disabled = disabledLite.reversed(),
	primary = cSecondary,
	primaryStrong = opaque(0xECEFF1), // blue-gray 50
	foreBad = opaque(0xFFAB91), // deep orange 200
	foreWarn = opaque(0xFFE082), // amber 200
	foreGood = opaque(0xA5D6A7), // green 200
	foreWeak = opaque(0xaaaaaa),
	todFmt = lightTheme.todFmt,
)


@Composable fun CAppTheme2(content: @Composable (AppTheme) -> Unit) {
	val theme = if(isSystemInDarkTheme()) darkTheme else lightTheme
	MaterialTheme(
		colors = theme.material,
		typography = appTypography,
		shapes = appShapes,
	) {
		content(theme)
	}
}