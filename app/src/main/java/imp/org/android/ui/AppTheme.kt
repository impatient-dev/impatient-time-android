package imp.org.android.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.material.Surface
import androidx.compose.material.Typography
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import imp.compose.ColorPair
import imp.compose.maxContrast
import imp.compose.style.CSDivider
import imp.compose.style.csBtnText
import imp.compose.style.csLink
import imp.compose.style.csText
import imp.compose.style.csTextBar
import imp.compose.under
import imp.util.fmt.TODFmt

private val family = FontFamily.SansSerif

val appTypography = Typography(
	body1 = csText(family, Color.Unspecified, 16.sp).style
)

val appShapes = Shapes(
	small = RoundedCornerShape(4.dp),
	medium = RoundedCornerShape(4.dp),
	large = RoundedCornerShape(0.dp),
)

/**Horizontal spacing to separate adjacent buttons.*/
val btnSep = 4.dp


class AppTheme(
	main: ColorPair,
	disabled: ColorPair,
	val primary: Color,
	primaryStrong: Color,
	foreBad: Color,
	foreWarn: Color,
	foreGood: Color,
	foreWeak: Color,
	val todFmt: TODFmt,
) {
	/**Background color for a dialog.*/
	val dialog = main.back

	val body = appTypography.body1.csText.copy(color = main.fore)
	val bodyBad = body.copy(color = foreBad)
	val bodyGood = body.copy(color = foreGood)
	val bodyWarn = body.copy(color = foreWarn)

	val strong = body.copy(weight = FontWeight.Bold)
	val strongBad = strong.copy(color = foreBad)
	val strongGood = strong.copy(color = foreGood)
	val strongWarn = strong.copy(color = foreWarn)
	val strongDisabled = strong.copy(color = foreWeak, fontStyle = FontStyle.Italic)

	val huge = body.copy(size = 48.sp)
	val hugeBad = huge.copy(color = foreBad)
	val hugeWarn = huge.copy(color = foreWarn)
	val hugeGood = huge.copy(color = foreGood)

	val titleSmall = csTextBar(family, primaryStrong.under(), 20.sp)

	val link = body.csLink(disabledColor = foreWeak, hpad = 4.dp)
	val btn = csBtnText(family, 16.sp, FontWeight.W500, primary.under(), disabled)
	val btnDanger = btn.copy(foreBad.under())

	val divider = CSDivider(main.fore)

	val material = Colors(
		primary = primary,
		primaryVariant = primary,
		secondary = primary,
		secondaryVariant = primary,
		background = main.back,
		surface = main.back,
		error = foreBad,
		onPrimary = primary.maxContrast(),
		onSecondary = primary.maxContrast(),
		onBackground = main.fore,
		onSurface = main.fore,
		onError = foreBad.maxContrast(),
		isLight = main.fore.maxContrast() == Color.White,
	)
}


private val LightColorPalette = lightColors(
	primary = Color(0xff37474f), // blue-gray 800
	primaryVariant = Color(0xff102027),
	secondary = Color(0xff90a4ae), // blue-gray 300
)

private val DarkColorPalette = darkColors(
	primary = LightColorPalette.primary,
	onPrimary = Color.White,
	primaryVariant = LightColorPalette.primaryVariant,
	secondary = LightColorPalette.secondary,
)

val colorSafe = Color(0xff008800)
val colorWarn = Color(0xffcc6600)

val styleSectionHeader = TextStyle(
	fontFamily = FontFamily.SansSerif,
	fontWeight = FontWeight.W400,
	fontSize = 24.sp,
)
/**For brief explanatory notes, e.g. "excludes inactive" next to a list of things that might not be active.*/
val styleNote = TextStyle(
	fontFamily = FontFamily.SansSerif,
	fontWeight = FontWeight.W200,
	fontSize = 12.sp,
	fontStyle = FontStyle.Italic,
)
/**A short piece of text (e.g. a title or number) that's much larger than normal.*/
val styleBig = TextStyle(
	fontFamily = FontFamily.SansSerif,
	fontWeight = FontWeight.W800,
	fontSize = 28.sp,
)
/**An extremely short piece of text (e.g. a number) that's much larger than normal.*/
val styleBigger = TextStyle(
	fontFamily = FontFamily.SansSerif,
	fontWeight = FontWeight.W800,
	fontSize = 36.sp,
)

/**Padding suitable for separating an invisible box from other nearby content.*/
val padInvisiBox = 8.dp


@Deprecated("")
@Composable fun ThemedSurface(content: @Composable () -> Unit) {
	Surface(color = MaterialTheme.colors.background) { content() }
}

@Deprecated("")
@Composable fun AppThemedSurface(content: @Composable () -> Unit) {
	CAppTheme {
		Surface(color = MaterialTheme.colors.background) {
			content()
		}
	}
}

@Deprecated("")
@Composable fun CAppTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
	val colors = if (darkTheme) DarkColorPalette else LightColorPalette

	MaterialTheme(
		colors = colors,
		typography = appTypography,
		shapes = appShapes,
		content = content
	)
}