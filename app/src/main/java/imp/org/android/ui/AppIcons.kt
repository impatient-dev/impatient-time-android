package imp.org.android.ui

import imp.org.android.R

/**This class mainly exists because Android Studio is bugged and thinks everything in R.drawable doesn't exist.
 * Putting all those references in this class means you don't see red underlines anywhere else.*/
object AppIcons {
	val arrowDown = R.drawable.arrow_down_thick

	val impliesLabel: Int = R.drawable.chevron_right_circle_outline
	val labelOverridden: Int = R.drawable.cancel
	val labelConflict: Int = R.drawable.source_branch_remove
}