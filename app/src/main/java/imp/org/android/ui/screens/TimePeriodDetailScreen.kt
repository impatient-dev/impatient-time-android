package imp.org.android.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import imp.compose.CColMaxScrollV
import imp.compose.CColWL
import imp.compose.CRowL
import imp.compose.CRowWL
import imp.compose.CSpinnerHC
import imp.compose.CSurfaceFull
import imp.org.android.time.ImpliedLabels
import imp.org.android.time.PastTimePeriodI
import imp.org.android.time.PossiblyImpliedLabel
import imp.org.android.ui.AppTheme
import imp.org.android.ui.widgets.CIconAndName
import imp.util.fmt.TimeLetterFmt
import java.time.Instant
import java.time.ZoneId

object TimePeriodDetailScreen {
	interface Handler {
		fun onClickCurrentTimePeriod()
		fun onClick(implication: PossiblyImpliedLabel)
	}

	@Composable fun Compose(
		period: PastTimePeriodI?,
		implications: ImpliedLabels?,
		theme: AppTheme,
		handler: Handler?,
		zone: ZoneId = ZoneId.systemDefault(),
		now: Instant = Instant.now(),
	) = CSurfaceFull {
		CColMaxScrollV {
			if(period == null || implications == null) {
				CSpinnerHC()
			} else {
				Content(period, implications, theme, handler, zone, now)
			}
		}
	}

	@Composable private fun Content(
		period: PastTimePeriodI,
		implications: ImpliedLabels,
		theme: AppTheme,
		handler: Handler?,
		zone: ZoneId,
		now: Instant,
	) {
		theme.body.C("started ${theme.todFmt.dateHm(period.start.atZone(zone))}")
		if(period.end != null)
			theme.body.C("ended ${theme.todFmt.dateHm(period.end.atZone(zone))}")
		else
			theme.body.C("has not ended")
		theme.body.C(TimeLetterFmt.duration2((period.end ?: now).toEpochMilli() - period.start.toEpochMilli()))
		theme.divider.CH()

		CColWL {
			// list implied
			implications.implied.forEach { implication ->
				CRowWL(Modifier.clickable { handler?.onClick(implication) }) {
					implication.CIconAndName(theme)
				}
			}

			// list conflicts with implied
			for(conflict in implications.conflictsWithImplied) {
				CRowL(Modifier.clickable { handler?.onClick(conflict) }) {
					conflict.CIconAndName(theme)
				}
			}

			// list conflicts with direct
			for(conflict in implications.conflictsWithDirect) {
				CRowL(Modifier.clickable { handler?.onClick(conflict) }) {
					conflict.CIconAndName(theme)
				}
			}
		}
	}
}