package imp.org.android.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import imp.compose.CColMaxScrollV
import imp.compose.CColWL
import imp.compose.CHPad
import imp.compose.CRowC
import imp.compose.CRowL
import imp.compose.CRowWL
import imp.compose.CSpinnerHC
import imp.compose.CSurfaceFull
import imp.org.android.time.CurrentTimePeriodI
import imp.org.android.time.ImpliedLabels
import imp.org.android.time.PossiblyImpliedLabel
import imp.org.android.ui.AppTheme
import imp.org.android.ui.btnSep
import imp.org.android.ui.widgets.CCurrentTimePeriod
import imp.org.android.ui.widgets.CIconAndName
import java.time.Instant
import java.time.ZoneId

object CurrentTimePeriodScreen {
	interface Handler {
		fun onClickCurrentTimePeriod()
		fun onClick(implication: PossiblyImpliedLabel)
		fun showHistory()
		fun showActivityReport()
	}

	@Composable fun Compose(
		current: CurrentTimePeriodI?,
		implications: ImpliedLabels?,
		theme: AppTheme,
		handler: Handler?,
		zone: ZoneId = ZoneId.systemDefault(),
		now: Instant = Instant.now(),
	) = CSurfaceFull {
		CColMaxScrollV {
			if(current == null || implications == null) {
				CSpinnerHC()
			} else {
				Content(current, implications, theme, handler, zone, now)
			}
		}
	}

	@Composable private fun Content(
		current: CurrentTimePeriodI,
		implications: ImpliedLabels,
		theme: AppTheme,
		handler: Handler?,
		zone: ZoneId = ZoneId.systemDefault(),
		now: Instant = Instant.now(),
	) {
		CCurrentTimePeriod.Compose(current, theme, zone = zone, now = now, onClick = { handler?.onClickCurrentTimePeriod() })
		theme.divider.CH()

		CColWL {
			// list implied
			implications.implied.forEach { implication ->
				CRowWL(Modifier.clickable { handler?.onClick(implication) }) {
					implication.CIconAndName(theme)
				}
			}

			// list conflicts with implied
			for(conflict in implications.conflictsWithImplied) {
				CRowL(Modifier.clickable { handler?.onClick(conflict) }) {
					conflict.CIconAndName(theme)
				}
			}

			// list conflicts with direct
			for(conflict in implications.conflictsWithDirect) {
				CRowL(Modifier.clickable { handler?.onClick(conflict) }) {
					conflict.CIconAndName(theme)
				}
			}
		}

		CRowC {
			theme.btn.C("History") { handler?.showHistory() }
			CHPad(btnSep)
			theme.btn.C("Activity Report") { handler?.showActivityReport() }
		}
	}
}