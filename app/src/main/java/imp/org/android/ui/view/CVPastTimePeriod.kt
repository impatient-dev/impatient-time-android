package imp.org.android.ui.view

import android.content.Context
import androidx.compose.runtime.Composable
import imp.compose.cRemember
import imp.compose.view.CVHelper
import imp.org.android.time.ImpliedLabels
import imp.org.android.time.PastTimePeriodI
import imp.org.android.time.TimePeriodRepo
import imp.org.android.time.calculateImpliedLabels
import imp.org.android.time.toPastInfo
import kotlinx.coroutines.CoroutineScope
import java.util.UUID

suspend fun _CVPastTimePeriod_load(id: UUID, ctx: Context): Pair<PastTimePeriodI, ImpliedLabels> {
	val period = TimePeriodRepo.get(id, ctx).toPastInfo(ctx)
	val implications = calculateImpliedLabels(period.labels, ctx, sortForUi = true)
	return Pair(period, implications)
}



/**Maintains an up-to-date view of a past time period, and the labels that are implied .*/
@Composable inline fun CVPastTimePeriod(
	id: UUID,
	ctx: Context,
	block: @Composable (PastTimePeriodI?, ImpliedLabels?, CoroutineScope) -> Unit,
) = CVHelper<Pair<PastTimePeriodI, ImpliedLabels>>(
	defaultLoad = { _CVPastTimePeriod_load(id, ctx) },
) { data, loading, scope, reload ->
	block(data?.first, data?.second, scope)

	TimePeriodRepo.listeners.cRemember { object : TimePeriodRepo.Listener {
		override suspend fun onTimePeriodChange() {
			reload()
		}
	}}
}