package imp.org.android.ui.view

import android.content.Context
import androidx.compose.runtime.Composable
import imp.compose.cRemember
import imp.compose.view.CVHelper
import imp.org.android.LabelRepo
import imp.org.android.time.CurrentTimePeriodI
import imp.org.android.time.ImpliedLabels
import imp.org.android.time.TimePeriodRepo
import imp.org.android.time.calculateImpliedLabels
import imp.util.epochMsToInstant
import imp.util.withDefault
import kotlinx.coroutines.CoroutineScope

suspend fun _CVCurrentTimePeriod_load(ctx: Context, skipImplications: Boolean): Pair<CurrentTimePeriodI, ImpliedLabels?> {
	val latest = TimePeriodRepo.latest(ctx)

	val period = if(latest == null) {
		CurrentTimePeriodI(id = -1, uuid = null, start = null, labels = emptyList())
	} else if(latest.endMs != null) {
		CurrentTimePeriodI(id = -1, uuid = null, start = latest.endMs!!.epochMsToInstant(), labels = emptyList())
	} else {
		val labels = latest.labels?.map { LabelRepo.get(it, ctx) } ?: emptyList()
		CurrentTimePeriodI(id = latest.id, uuid = latest.uuid, start = latest.startMs.epochMsToInstant(), labels = labels)
	}

	val implications = if(skipImplications) null else calculateImpliedLabels(period.labels, ctx, sortForUi = true)
	return Pair(period, implications)
}



/**Maintains an up-to-date view of the time period that's currently in progress, and the labels that are implied .*/
@Composable inline fun CVCurrentTimePeriod(
	ctx: Context,
	skipImplications: Boolean = false,
	block: @Composable (CurrentTimePeriodI?, ImpliedLabels?, CoroutineScope) -> Unit,
) = CVHelper<Pair<CurrentTimePeriodI, ImpliedLabels?>>(
	defaultLoad = { _CVCurrentTimePeriod_load(ctx, skipImplications = skipImplications) },
) { data, loading, scope, reload ->
	block(data?.first, data?.second, scope)

	TimePeriodRepo.listeners.cRemember { object : TimePeriodRepo.Listener {
		override suspend fun onTimePeriodChange() {
			reload()
		}
	}}
}