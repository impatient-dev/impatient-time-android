package imp.org.android.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.snapshots.SnapshotStateList
import imp.compose.CColMax
import imp.compose.CColMaxScrollV
import imp.compose.CRadioW
import imp.compose.CRowWC
import imp.compose.CSurfaceFull
import imp.compose.CWeightBoxW
import imp.compose.CWeightColL
import imp.compose.rcms
import imp.compose.rcmslWith
import imp.org.android.LabelI
import imp.org.android.time.CurrentTimePeriodI
import imp.org.android.ui.AppTheme
import imp.util.addSortedBy
import imp.util.minusAll


/**Allows the user to edit the current time period or start a new one.*/
object EditCurrentTimePeriodScreen {
	interface Handler {
		/**Saves changes.
		 * @param new whether to create a new time period, instead of editing the current one
		 * @param startAtSubmit only relevant for new time periods*/
		fun saveTimePeriod(new: Boolean, startAtSubmit: Boolean, labels: List<LabelI>, )
	}

	@Composable fun Compose(
		current: CurrentTimePeriodI,
		labels: List<LabelI>,
		theme: AppTheme,
		handler: Handler?,
	) = CSurfaceFull {

		val new = rcms(true)
		val startAtSubmit = rcms(true)
		val chosenLabels = rcmslWith(current.labels)
		val availableLabels = rcmslWith(labels
			.minusAll { it.notDirect || chosenLabels.contains(it) }
			.sortedBy { it.name }
		)

		CColMax {
			CWeightBoxW {
				MainContent(current, theme,
					new = new,
					startAtSubmit = startAtSubmit,
					chosenLabels = chosenLabels,
					availableLabels = availableLabels,
				)
			}
			BottomButtons(theme, handler,
				new = new,
				startAtSubmit = startAtSubmit,
				chosenLabels = chosenLabels,
			)
		}
	}



	@Composable private fun MainContent(
		current: CurrentTimePeriodI,
		theme: AppTheme,
		new: MutableState<Boolean>,
		startAtSubmit: MutableState<Boolean>,
		chosenLabels: SnapshotStateList<LabelI>,
		availableLabels: SnapshotStateList<LabelI>,
	) = CColMaxScrollV {
		theme.body.CRadioW("Start a new time period", new.value) { new.value = true }
		theme.body.CRadioW("Edit the current time period", !new.value, enabled = current.uuid != null) { new.value = false }
		theme.divider.CH()

		if(new.value) {
			theme.strong.C("Start")
			theme.body.CRadioW("when this screen opened", !startAtSubmit.value) { startAtSubmit.value = false }
			theme.body.CRadioW("when this screen closes", startAtSubmit.value) { startAtSubmit.value = true }
			theme.divider.CH()
		}

		theme.strong.C("Labels (${chosenLabels.size})")
		if(chosenLabels.isEmpty())
			theme.bodyWarn.C("none")
		chosenLabels.forEachIndexed { i, label ->
			CChosenLabel(label, theme, remove = { availableLabels.addSortedBy(chosenLabels.removeAt(i)) { it.name } })
		}
		theme.divider.CH()

		theme.strong.C("Available Labels")
		availableLabels.forEachIndexed { i, label ->
			CAvailableLabel(label, theme, choose = { chosenLabels.add(availableLabels.removeAt(i)) })
		}
	}


	@Composable private fun BottomButtons(
		theme: AppTheme,
		handler: Handler?,
		new: MutableState<Boolean>,
		startAtSubmit: MutableState<Boolean>,
		chosenLabels: SnapshotStateList<LabelI>,
	) {
		val submitted = rcms(false)
		theme.divider.CH()
		theme.btn.C("Submit", enabled = !submitted.value) {
			handler?.saveTimePeriod(
				new = new.value,
				startAtSubmit = startAtSubmit.value,
				labels = ArrayList(chosenLabels),
			)
			submitted.value = true
		}
	}


	@Composable private fun CChosenLabel(
		label: LabelI,
		theme: AppTheme,
		remove: () -> Unit,
	) = CRowWC {
		CWeightColL {
			theme.body.C(label.name)
			label.description?.let { theme.body.C(it) }
		}
		theme.btn.C("Remove", onClick = remove)
	}

	@Composable private fun CAvailableLabel(
		label: LabelI,
		theme: AppTheme,
		choose: () -> Unit,
	) = CRowWC {
		CWeightColL {
			theme.body.C(label.name)
			label.description?.let { theme.body.C(it) }
		}
		theme.btn.C("Add", onClick = choose)
	}
}