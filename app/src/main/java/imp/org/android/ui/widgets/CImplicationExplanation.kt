package imp.org.android.ui.widgets

import androidx.compose.runtime.Composable
import imp.android.compose.CIcon
import imp.compose.CColWC
import imp.compose.CRowL
import imp.org.android.LabelI
import imp.org.android.time.DirectlyAppliedLabel
import imp.org.android.time.ImpliedConflictLabel
import imp.org.android.time.ImpliedLabel
import imp.org.android.time.PossiblyImpliedLabel
import imp.org.android.ui.AppIcons
import imp.org.android.ui.AppTheme

@Composable fun CImplicationExplanation(imp: PossiblyImpliedLabel, theme: AppTheme) = CColWC {
	theme.body.C("label")
	imp.CIconAndName(theme)

	if(imp is DirectlyAppliedLabel) {
		theme.body.C("was directly applied")
	} else if(imp is ImpliedLabel) {
		theme.body.C("was implied through this chain:")
		theme.divider.CH()
		CChain(imp.chain, theme)
	} else if(imp is ImpliedConflictLabel) {
		theme.body.C("was not implied because it would conflict with")
		val conflict = imp.mostDirectConflictingChain()
		if(conflict.size > 1)
			theme.divider.CH()
		CChain(conflict, theme)
		if(conflict.size > 1)
			theme.divider.CH()
		CRowL {
			theme.body.C("both are part of exclusive set: ")
			theme.strong.C(imp.conflict.set.name)
		}
	}
}


@Composable private fun CChain(chain: List<LabelI>, theme: AppTheme) = CColWC {
	chain.forEachIndexed { i, lbl ->
		if(i > 0)
			theme.primary.CIcon(AppIcons.arrowDown)
		theme.body.C(lbl.name)
	}
}


private fun icon(imp: PossiblyImpliedLabel): Int? = when(imp) {
	is DirectlyAppliedLabel -> null
	is ImpliedLabel -> AppIcons.impliesLabel
	is ImpliedConflictLabel -> if(imp.conflictsWithDirectlyApplied()) AppIcons.labelOverridden else AppIcons.labelConflict
}

@Composable fun PossiblyImpliedLabel.CIconAndName(theme: AppTheme) = CRowL {
	val i = icon(this@CIconAndName)
	if(i != null)
		theme.primary.CIcon(i)
	theme.body.C(' ' + label.name)
}