package imp.org.android.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import imp.compose.CCheckW
import imp.compose.CColMaxScrollV
import imp.compose.CRowWC
import imp.compose.CRowWL
import imp.compose.CSpinner
import imp.compose.CSpinnerHC
import imp.compose.CSurfaceFull
import imp.compose.CTextField
import imp.compose.CWeight
import imp.compose.cms
import imp.compose.cmsn
import imp.compose.rcms
import imp.org.android.mgmt.DataCleanupReport
import imp.org.android.todFmt
import imp.org.android.ui.AppTheme
import imp.util.exToNull
import imp.util.fmt.parseDateTime
import imp.util.toInstant
import imp.util.toLocal
import java.time.Instant
import java.time.LocalDateTime

/**Tells the user how much data is stored and allows him to delete some or all of it.*/
class TimeDataCleanupScreen {
	private val confirmedDeleteBefore = cmsn<Instant>()
	private val deleteInProgress = cms(false)

	interface Handler {
		fun requestRegenReport(deleteBefore: Instant?)
		/**This function must call [onFinishedDeleting] once the delete finishes.*/
		fun launchDeleteProcess(deleteBefore: Instant)
	}

	fun onFinishedDeleting() {
		deleteInProgress.value = false
		confirmedDeleteBefore.value = null
	}

	@Composable fun Compose(
		report: DataCleanupReport?,
		loadingReport: Boolean,
		theme: AppTheme,
		handler: Handler?,
	) = CSurfaceFull {
		CColMaxScrollV {
			if(report == null) {
				CSpinnerHC()
			} else {
				Content(report, loadingReport, theme, handler)
			}
		}
	}


	@Composable private fun Content(
		report: DataCleanupReport,
		loadingReport: Boolean,
		theme: AppTheme,
		handler: Handler?,
	) {
		// basic report
		val earliestStart = report.earliestTimePeriodStart
		val startingAtStr = remember(earliestStart) {
			if(earliestStart == null) "never" else todFmt.dateHm(earliestStart.toLocal())
		}
		CRowWC {
			theme.body.C("There are ${report.timePeriods} time periods stored, starting at $startingAtStr")
			if(loadingReport) {
				CWeight()
				CSpinner()
			}
		}
		theme.divider.CH()

		val deleteCutoffStr = rememberSaveable { cms("") }
		val deleteCutoff = rcms { parseDeleteCutoff(deleteCutoffStr.value) }
		val dc = deleteCutoff.value

		// enter the delete cutoff
		theme.body.C("Delete data older than")
		CRowWL {
			CTextField(deleteCutoffStr.value,
				label = "date / time",
				placeholder = "",
				enabled = !deleteInProgress.value,
				isError = deleteCutoffStr.value.isNotBlank() && dc == null,
			) {
				deleteCutoffStr.value = it
				deleteCutoff.value = parseDeleteCutoff(it)
				confirmedDeleteBefore.value = null
			}
		}

		if (dc == null) {
			// nothing
		} else if(report.deleteProposal?.deleteBefore != dc.instant) {
			// ask to regen report
			LaunchedEffect(dc.instant) {
				handler?.requestRegenReport(dc.instant)
			}
			CSpinner()
		} else {
			// checkbox to confirm deletion is ok
			theme.body.CCheckW(
				"Delete ${report.deleteProposal.timePeriodsToDelete} time periods and keep ${report.timePeriodsAfterDelete}.",
				checked = confirmedDeleteBefore.value == report.deleteProposal.deleteBefore,
			) {
				confirmedDeleteBefore.value = if(confirmedDeleteBefore.value == report.deleteProposal.deleteBefore) null else report.deleteProposal.deleteBefore
			}
		}

		// delete button, which shows a spinner while deleting
		if(deleteInProgress.value) {
			theme.btnDanger.C(enabled = false, onClick = {}, content = { CSpinner() })
		} else {
			theme.btnDanger.C("Delete",
				enabled = dc != null && confirmedDeleteBefore.value == dc.instant,
			) {
				if(handler != null) {
					handler.launchDeleteProcess(dc!!.instant)
					deleteInProgress.value = true
				}
			}
		}
	}
}


@Immutable private data class LocalInstant(
	val local: LocalDateTime,
	val instant: Instant,
)

private fun parseDeleteCutoff(str: String): LocalInstant? = exToNull {
	if(str.isBlank())
		return null
	val local = parseDateTime(str, future = false)
	LocalInstant(local, local.toInstant())
}