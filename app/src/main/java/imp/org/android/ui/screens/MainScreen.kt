package imp.org.android.ui.screens

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.runtime.Composable
import imp.compose.CColMaxScrollV
import imp.compose.CSurfaceFull
import imp.org.android.charge.ChargeActivity
import imp.org.android.multitimer.MultistepTimerActivity
import imp.org.android.time.CurrentTimePeriodActivity
import imp.org.android.time.TimeDataCleanupActivity
import imp.org.android.ui.AppTheme
import kotlin.reflect.KClass

object MainScreen {
	interface Handler {
		fun openActivity(cls: KClass<out Activity>)
	}

	@Composable fun Compose(theme: AppTheme, handler: Handler?) = CSurfaceFull {
		CColMaxScrollV(vertical = Arrangement.Center) {
			theme.btn.C("Current Activity") { handler?.openActivity(CurrentTimePeriodActivity::class)}
			theme.btn.C("Clean-Up Old Data") { handler?.openActivity(TimeDataCleanupActivity::class)}
			theme.btn.C("Multi-Step Timer") { handler?.openActivity(MultistepTimerActivity::class)}
			theme.btn.C("Battery Alerts") { handler?.openActivity(ChargeActivity::class)}
		}
	}
}