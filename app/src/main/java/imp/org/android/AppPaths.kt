package imp.org.android

import android.content.Context
import imp.json.impJackson
import imp.sqlite.Database
import imp.sqlite.android.AndroidSqliteDatabase
import imp.sqlite.cache.ArgDbCache1Only
import imp.time.data.orm
import imp.util.closeOnFail
import imp.util.fmt.TOD12
import imp.util.logger

private val log = AppPaths::class.logger
val jackson = impJackson()

object AppPaths {
	fun chargeLimitsJson(ctx: Context) = ctx.filesDir.resolve("chargeLimits.json")
	fun chargeNotificationJson(ctx: Context) = ctx.filesDir.resolve("chargeNotification.json")
	fun timePeriodsJson(ctx: Context) = ctx.filesDir.resolve("timePeriods.json")
}

val dbc = ArgDbCache1Only<Context>(::openDbInitIfNecessary, ttlMs = 30_000)

private var dbInit = false

private fun openDbInitIfNecessary(context: Context): Database = AndroidSqliteDatabase(context, "imp-org").closeOnFail { db ->
	if(!dbInit) {
		log.debug("Updating database schema.")
		orm.applySchema(db)
		dbInit = true
	}
}