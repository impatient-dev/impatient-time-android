package imp.org.android.start

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import imp.android.impStartActivity
import imp.org.android.ui.CAppTheme2
import imp.org.android.ui.screens.MainScreen
import kotlin.reflect.KClass


class MainActivity : MainScreen.Handler, ComponentActivity() {
	override fun onCreate(savedState: Bundle?) {
		super.onCreate(savedState)
		setContent {
			CAppTheme2 { theme ->
				MainScreen.Compose(theme, this)
			}
		}
	}

	override fun openActivity(cls: KClass<out Activity>) = impStartActivity(cls)
}
