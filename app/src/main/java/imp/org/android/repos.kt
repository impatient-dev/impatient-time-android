package imp.org.android

import android.content.Context
import imp.repo.sqlite.MemoryTableRepo
import imp.sqlite.Database
import imp.sqlite.android.AndroidSqliteDatabase
import imp.sqlite.cache.ArgDbCache1Only
import imp.time.data.LabelD
import imp.time.data.loader.LabelLoader
import imp.time.data.loader.TimePeriodLoader
import imp.time.data.orm
import imp.util.closeOnFail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.time.Duration

@Volatile private var initialized = false
private var dbInitMutex = Mutex()




/**Opens a database connection using the provided context, or the global application context if you don't provide one.
 * This shouldn't be called on the main thread.*/
suspend fun openDb(context: Context = globalContext): Database = AndroidSqliteDatabase(context, "imp-time").closeOnFail { db ->
	if (!initialized) {
		dbInitMutex.withLock {
			if (!initialized) {
				orm.applySchema(db)
				initialized = true
			}
		}
	}
}

/**Switches to the IO dispatcher, then opens a database connection and calls the provided function.*/
suspend fun <R> Context.withDb(block: suspend (Database) -> R): R {
	val context = this
	return withContext(Dispatchers.IO) {
		openDb(context).use { db ->
			block(db)
		}
	}
}


@Deprecated("") val labelLoader = LabelLoader(Duration.ofMinutes(1), ::openDb)
val timePeriodLoader = TimePeriodLoader(::openDb)