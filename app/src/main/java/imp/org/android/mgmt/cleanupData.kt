package imp.org.android.mgmt

import android.content.Context
import androidx.compose.runtime.Immutable
import imp.org.android.time.TimePeriodRepo
import imp.util.epochMsToInstant
import java.time.Instant


@Immutable class DataCleanupReport (
	val earliestTimePeriodStart: Instant?,
	val timePeriods: Long,
	val deleteProposal: DeleteOldDataProposal?,
) {
	val timePeriodsAfterDelete get() = timePeriods - (deleteProposal?.timePeriodsToDelete ?: 0)
}

@Immutable class DeleteOldDataProposal (
	val deleteBefore: Instant,
	val timePeriodsToDelete: Long,
)


suspend fun prepareDataCleanupReport(deleteOlderThan: Instant?, ctx: Context): DataCleanupReport {
	val allPeriods = TimePeriodRepo.getAllEarliestFirst(ctx)
	val start1 = allPeriods.firstOrNull()?.startMs?.epochMsToInstant()
	val deleteProposal = deleteOlderThan?.let { cutoff -> DeleteOldDataProposal(cutoff,
		timePeriodsToDelete = allPeriods.count { TimePeriodRepo.isOlderThan(it, cutoff) }.toLong())
	}
	return DataCleanupReport(earliestTimePeriodStart = start1, timePeriods = allPeriods.size.toLong(), deleteProposal = deleteProposal)
}