package imp.org.android

import imp.org.android.hardcoded.defineHardcodedData
import imp.time.data.LabelD
import imp.util.bitwiseOr
import imp.util.blankToNull
import imp.util.hashedUuid
import imp.util.putNew
import java.util.UUID

val hardcodedData = defineHardcodedData()


class HardcodedDataBuilder {
	private val labels = HashMap<String, LabelToHardcode>()
	private val activities = ArrayList<TrackedActivityToHardcode>()

	/**The uniqueName is hashed to a UUID and must not change.*/
	fun label(
		name: String,
		uniqueName: String,
		description: String = "",
		implies: Set<String> = emptySet(),
		flags: Set<LabelI.Flag> = emptySet(),
	) = this.also {
		labels.putNew(uniqueName, LabelToHardcode(name = name, uniqueName = uniqueName, description = description.blankToNull(), implies = implies, flags = flags)) {
			"Duplicate label uniqueName: $uniqueName"
		}
	}

	fun activity(
		name: String,
		anyLabel: List<String>,
	) = this.also {
		activities.add(TrackedActivityToHardcode(name, anyLabel))
	}


	/**Includes validation.*/
	fun build(): HardcodedData {
		// put all labels without implies-relationships
		val outLabels = HashMap<UUID, HardcodedLabel>(labels.size)
		labels.values.forEach { label ->
			val flags = label.flags.asSequence().map { it.bit }.bitwiseOr()
			outLabels.putNew(label.id, HardcodedLabel(label.id, name = label.name, description = label.description, implies = emptySet(), flags = flags))
		}

		// add implies-relationships to labels
		for(def in labels.values) {
			if(def.implies.isEmpty())
				continue
			val id = def.uniqueName.hashedUuid()
			val implies = def.implies.map { im ->
				val found = outLabels[im.hashedUuid()] ?: throw Exception("Label ${def.name} implies nonexistent label $im")
				found.id
			}
			outLabels.compute(id) { _, label -> label!!.copy(implies = implies) }
		}

		// put activities
		val outActivities = HashMap<String, TrackedActivity>(activities.size)
		for(act in activities) {
			val activityLabels = act.anyLabel.map { lblName ->
				val src = labels[lblName] ?: throw Exception("For activity ${act.name}, no such label: $lblName")
				src.id
			}
			outActivities.putNew(act.name, TrackedActivity(name = act.name, anyLabel = activityLabels)) { "Duplicate activity name: ${act.name}" }
		}

		return HardcodedData(labels = outLabels, activities = outActivities)
	}
}




data class HardcodedData (
	val labels: Map<UUID, HardcodedLabel>,
	val activities: Map<String, TrackedActivity>,
)


private class LabelToHardcode (
	val name: String,
	val uniqueName: String,
	val description: String?,
	val implies: Set<String>,
	val flags: Set<LabelI.Flag>,
) {
	val id = uniqueName.hashedUuid()
}

data class HardcodedLabel (
	val id: UUID,
	val name: String,
	val description: String?,
	val flags: Int,
	val implies: Collection<UUID>,
) {
	fun toData() = LabelD(
		id = -1,
		uuid = id,
		name = name,
		description = description,
		flags = flags,
	)
}


private class TrackedActivityToHardcode (
	val name: String,
	val anyLabel: List<String>,
) {
	init {
		check(anyLabel.isNotEmpty()) { "No labels for activity: $name" }
	}
}

class TrackedActivity (
	val name: String,
	val anyLabel: List<UUID>,
)