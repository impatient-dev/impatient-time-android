package imp.org.android

import android.content.Context
import java.lang.IllegalStateException

private var _context: Context? = null

/**Returns a Context, for use in situations where you don't have access to one.
 * This should only be accessed from the main thread.*/
@Deprecated("") val globalContext: Context get() = _context ?: throw IllegalStateException()//TODO

/**Call this every time a new Context is created (e.g. in Activity.onCreate()), so we're always sure to have one available.
 * This should only be accessed from the main thread.*/
fun onNewContext(context: Context) {
	//always replace the previous context, so we don't hold onto Activities, etc. longer than necessary
	_context = context
}