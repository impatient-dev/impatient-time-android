package imp.org.android

import android.os.Bundle
import android.text.format.DateFormat
import android.view.WindowManager
import androidx.activity.ComponentActivity
import imp.util.fmt.TOD12
import imp.util.fmt.TOD24
import imp.util.fmt.TODFmt
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel


var todFmt: TODFmt = TOD12
private var initialized = false


/**Parent class for activities in this app.
 * Maintains a coroutine scope, and sets some useful UI defaults.
 * You're still required to call the superclass method when you override a lifecycle method (e.g. onCreate).*/
abstract class AppActivity : ComponentActivity() {
	/**A main-dispatcher scope that is cancelled when the activity is destroyed.*/
	val scope: CoroutineScope = MainScope()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		onNewContext(this)
		if(!initialized) {
			todFmt = if(DateFormat.is24HourFormat(this)) TOD24 else TOD12
			initialized = true
		}
		this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
	}

	override fun onDestroy() {
		super.onDestroy()
		scope.cancel()
	}
}
