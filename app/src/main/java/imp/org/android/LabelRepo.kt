package imp.org.android

import android.content.Context
import imp.org.android.time.checkNoLabelImplicationCycle
import imp.time.data.LabelD
import imp.util.SusRepoAllD
import imp.util.mapToHashSet
import imp.util.require
import java.util.UUID


/**All labels are currently hardcoded and cannot be changed.*/
object LabelRepo {
	suspend fun getAll(ctx: Context): List<LabelI> = InnerRepo.get(ctx).all
	suspend fun getAllByUuid(ctx: Context): Map<UUID, LabelI> = InnerRepo.get(ctx).byUuid
	suspend fun get(id: UUID, ctx: Context): LabelI = InnerRepo.get(ctx).byUuid[id] ?: throw Exception("Label not found: $id")
	suspend fun toInfo(label: LabelD, ctx: Context): LabelI = get(label.uuid, ctx)


	private class AllLabels (
		val byUuid: Map<UUID, LabelI>,
		val all: List<LabelI>,
	)

	private object InnerRepo : SusRepoAllD<LabelI, AllLabels, Context>() {
		override suspend fun load(d: Context): AllLabels {
			val hardcoded = hardcodedData.labels
			val datas = hardcoded.values.associateBy({ it.id }) { it.toData() }

			val impliedBy = HashMap<UUID, HashSet<LabelD>>()
			for(label in datas.values) {
				val h = hardcoded.require(label.uuid)
				for(implied in h.implies) {
					impliedBy.compute(implied) { _, s ->
						val ss = s ?: HashSet<LabelD>()
						ss.add(label)
						ss
					}
				}
			}

			val infos = hardcoded.values.associateBy({ it.id }) { LabelI(
				id = -1,
				uuid = it.id,
				name = it.name,
				description = it.description,
				flags = it.flags,
				implies = it.implies.mapToHashSet { implied -> datas[implied]!! },
				impliedBy = impliedBy[it.id] ?: emptySet(),
			) }

			checkNoLabelImplicationCycle(infos.values)
			return AllLabels(byUuid = infos, all = infos.values.toList())
		}

		override suspend fun with(current: AllLabels, item: LabelI, d: Context): AllLabels {
			throw UnsupportedOperationException()
		}

		override suspend fun without(current: AllLabels, item: LabelI, d: Context): AllLabels {
			throw UnsupportedOperationException()
		}
	}
}