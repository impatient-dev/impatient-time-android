package imp.org.android.multitimer

import androidx.compose.runtime.Immutable

/**Must be treated as immutable - all changes must allocate new objects. Never has 0 steps.*/
@Immutable data class MultistepTimerCfg (
	val steps: ArrayList<MultistepTimerStep> = arrayListOf(MultistepTimerStep()),
) {
	val totalSeconds: Int = steps.sumOf {it.seconds}
	val isValid: Boolean = steps.isNotEmpty() && !steps.any {it.seconds <= 0}
}

@Immutable data class MultistepTimerStep (
	val text: String = "",
	val seconds: Int = 0
) {
	val millis: Long get() = seconds * 1000L
}


fun MultistepTimerCfg.withStepAdded(step: MultistepTimerStep = MultistepTimerStep(), idx: Int = steps.size): MultistepTimerCfg =
	this.copy(
		steps = ArrayList<MultistepTimerStep>(steps.size + 1).also {
			it.addAll(steps)
			it.add(idx, step)
		}
	)

fun MultistepTimerCfg.withStepRemoved(idx: Int): MultistepTimerCfg {
	val list = ArrayList<MultistepTimerStep>()
	steps.forEachIndexed { i, step ->
		if(i != idx)
			list.add(step)
	}
	return this.copy(steps = list)
}

/**The block must copy the step.*/
fun MultistepTimerCfg.withStepEdited(idx: Int, block: (old: MultistepTimerStep) -> MultistepTimerStep): MultistepTimerCfg {
	val newStep = block(steps[idx])
	return this.copy(
		steps = ArrayList(steps).also { it[idx] = newStep }
	)
}

fun MultistepTimerCfg.withStepMoved(from: Int, to: Int): MultistepTimerCfg {
	val list = ArrayList(steps)
	val step = list.removeAt(from)
	val i = if(from + 1 < to) to - 1 else to //TODO move to util
	list.add(i, step)
	return this.copy(steps = list)
}