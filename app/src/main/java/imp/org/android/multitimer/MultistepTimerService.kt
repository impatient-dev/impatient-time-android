package imp.org.android.multitimer

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.compose.runtime.Immutable
import java.lang.Long.max
import java.lang.Long.min
import java.lang.System.currentTimeMillis


/**Must be treated as immutable, for use with Compose.*/
@Immutable data class MultistepTimerState (
	/**Index of the step we're currently counting down.*/
	val step: Int,
	/**Time when we started counting down this step.*/
	val startMs: Long,
	/**Time when we paused, or null if we're still counting.*/
	val pauseMs: Long?,
) {
	/**How many milliseconds have been counted down for this step.*/
	fun elapsedMs(now: Long = currentTimeMillis()) = (pauseMs ?: now) - startMs
	/**How many milliseconds are left until the current step completes.*/
	fun remainingMs(step: MultistepTimerStep, now: Long = currentTimeMillis()) = step.seconds * 1000 - elapsedMs(now)
}



class MultistepTimerService : Service() {
	override fun onBind(intent: Intent): MultistepTimerBinder = MultistepTimerThread

	override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
		return super.onStartCommand(intent, flags, startId)
	}

	override fun onDestroy() {
		super.onDestroy()
		MultistepTimerThread.stop()
	}
}

interface MultistepTimerBinder : IBinder {
	fun startCounting(config: MultistepTimerCfg)
	/**Taking the last time provided to the UI prevents the display from jumping when the user pauses - it feels a bit better.*/
	fun pause(nowMs: Long)
	fun unpause()
	fun stop()

	fun loadCurrent(block: (MultistepTimerCfg?, MultistepTimerState?) -> Unit)
	/**Registers a listener to be notified as we count down the timer.
	 * @param periodMs how often to call tick() when we're counting down.
	 * There can only be 1 listener at a time - this function deregisters any previous listener.
	 * Ticks will not occur while counting is paused or stopped.*/
	fun register(listener: MultistepTimerListener, periodMs: Long)
	fun deregister()
}

/**All these methods must be called on the main (not background) thread.*/
interface MultistepTimerListener {
	fun onTick(nowMs: Long)
	fun onStateChange(state: MultistepTimerState?)
}



private object MultistepTimerThread : MultistepTimerBinder, Binder() {
	private var config: MultistepTimerCfg? = null
	private var state: MultistepTimerState? = null
		set(value) {
			field = value
			handler?.post { listener?.onStateChange(value) }
		}
	private var running = false
	@Volatile private var stop = false

	private var listener: MultistepTimerListener? = null
	private var handler: Handler? = null
	private var listenerPeriodMs: Long = 0

	private val sync = Object()

	override fun loadCurrent(block: (MultistepTimerCfg?, MultistepTimerState?) -> Unit) {
		synchronized(sync) {
			block(config, state)
		}
	}

	override fun startCounting(config: MultistepTimerCfg) {
		val now = currentTimeMillis()
		synchronized(sync) {
			this.config = config
			this.state = MultistepTimerState(0, now, null)
			startOrWake()
		}
	}
	override fun pause(nowMs: Long) {
		synchronized(sync) {
			val s = state ?: return
			state = s.copy(pauseMs = nowMs)
		}
	}
	override fun unpause() {
		val now = currentTimeMillis()
		synchronized(sync) {
			val s = state ?: return
			state = s.copy(startMs = now - s.elapsedMs(now), pauseMs = null)
			startOrWake()
		}
	}
	override fun stop() {
		stop = true
		synchronized(sync) {
			config = null
			state = null
		}
	}

	override fun register(listener: MultistepTimerListener, periodMs: Long) {
		if(periodMs <= 0)
			throw IllegalArgumentException("Invalid period: $periodMs")
		synchronized(sync) {
			if(handler == null)
				handler = Handler(Looper.getMainLooper())
			this.listenerPeriodMs = periodMs
			this.listener = listener
			startOrWake()
		}
	}

	override fun deregister() {
		synchronized(sync) {
			listener = null
		}
	}


	private fun doNextThingInBackground() {
		val sleep: Long
		synchronized(sync) {
			sleep = if(config == null || state == null || state!!.pauseMs != null)
				1_000_000_000
			else {
				val now = currentTimeMillis()
				var step = config!!.steps[state!!.step]
				val end = state!!.startMs + step.millis
				val li = listener
				if(end > now) { //not finished with this step
					if(li != null)
						handler?.post { li.onTick(now)}
					listenerPeriodMs
				} else { //switch to the next step
					val carryOverMs = min(5000, -state!!.remainingMs(step, now))
					if(carryOverMs < 0) throw Exception("carried over $carryOverMs ms")
					val newStartMs = now - carryOverMs
					val i = (state!!.step + 1) % config!!.steps.size
					step = config!!.steps[i]
					state = state!!.copy(step = i, startMs = newStartMs)
					if(listener == null) state!!.remainingMs(step, now) - 1 else listenerPeriodMs
				}
			}
			sync.wait(max(100, sleep))
		}
	}

	private fun runInBackground() {
		while(!stop) {
			try {
				doNextThingInBackground()
			} catch(e: Exception) {
				e.printStackTrace()
				Thread.sleep(5_000)
			} finally {
				running = false
			}
		}
	}

	private fun startOrWake() {
		synchronized(sync) {
			if(running)
				sync.notifyAll()
			else {
				stop = false
				val thread = Thread({runInBackground()}, "multi-timer")
				thread.name = "mullti-timer"
				thread.isDaemon = false
				thread.start()
				running = true
			}
		}
	}
}