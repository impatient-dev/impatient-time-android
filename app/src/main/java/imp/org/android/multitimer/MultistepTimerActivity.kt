package imp.org.android.multitimer

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.key
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import imp.addOrClearFlags
import imp.org.android.ui.CAppTheme
import java.lang.Integer.parseInt
import java.lang.System.currentTimeMillis


class MultistepTimerActivity : MultiTimerConfigurable, MultistepTimerListener, ServiceConnection, ComponentActivity() {
	private var cfg = MultistepTimerCfg()
	/**If null, we're in edit mode and aren't timing anything.*/
	private var state: MultistepTimerState? = null
	private var binder: MultistepTimerBinder? = null
	private var nowMs: Long = currentTimeMillis()

	override fun onCreate(savedState: Bundle?) {
		super.onCreate(savedState)
		display()
	}

	override fun onStart() {
		super.onStart()
		bindService(Intent(this, MultistepTimerService::class.java), this, Context.BIND_AUTO_CREATE)
	}

	override fun onStop() {
		super.onStop()
		unbindService(this)
	}

	override fun onServiceConnected(cn: ComponentName, binder: IBinder) {
		this.binder = binder as MultistepTimerBinder
		binder.loadCurrent { c, s ->
			replaceState(s)
			if(s != null && c != null)
				replaceCfg { c }
		}
		binder.register(this, 1000L / 8)
	}

	override fun onServiceDisconnected(cn: ComponentName) {
		binder?.deregister()
	}

	override fun onStateChange(state: MultistepTimerState?) {
		replaceState(state)
	}

	override fun onTick(nowMs: Long) {
		this.nowMs = nowMs
		if(state?.pauseMs == null)
			display()
	}

	private fun display() {
		setContent { MyContent(cfg, state, nowMs, this) }
	}

	override fun replaceCfg(block: (old: MultistepTimerCfg) -> MultistepTimerCfg) {
		cfg = block(cfg)
		display()
	}
	private fun replaceState(state: MultistepTimerState?) {
		if(this.state === state)
			return
		this.state = state
		display()
	}
	override fun btnStartStop() {
		if(binder == null)
			return
		if(state == null) {
			binder!!.startCounting(cfg)
		} else {
			binder!!.stop()
		}
		window.addOrClearFlags(state != null, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
	}
	/**nowMs is only for pausing - providing this prevents a jump after the user hits the button*/
	override fun btnPauseResume(nowMs: Long) {
		if(binder == null || state == null)
			return
		if(state!!.pauseMs == null)
			binder!!.pause(nowMs)
		else
			binder!!.unpause()
	}
}

/**Exists so previews work without a real activity.*/
private interface MultiTimerConfigurable {
	/**The block must not change any existing objects.*/
	fun replaceCfg(block: (old: MultistepTimerCfg) -> MultistepTimerCfg)
	fun btnStartStop()
	fun btnPauseResume(nowMs: Long)
}

private inline fun onEditSeconds(idx: Int, newSecondsTxt: String, tgt: MultiTimerConfigurable) {
	val seconds: Int
	try {
		seconds = if(newSecondsTxt.isEmpty()) 0 else parseInt(newSecondsTxt)
	} catch(e: java.lang.NumberFormatException) {
		return
	}
	tgt.replaceCfg { c0 -> c0.withStepEdited(idx) { s0 -> s0.copy(seconds = seconds) } }
}


private object PreviewConfigurable : MultiTimerConfigurable {
	override fun replaceCfg(block: (MultistepTimerCfg) -> MultistepTimerCfg) {}
	override fun btnStartStop() {}
	override fun btnPauseResume(nowMs: Long) {}
}
private fun previewCfg() = MultistepTimerCfg()
	.withStepEdited(0) { it.copy(seconds = 10, text = "short step") }
	.withStepAdded(MultistepTimerStep(seconds = 80, text = "long step"))
private fun stateCounting() = MultistepTimerState(0, currentTimeMillis() - 5000, null)

@Preview(showBackground = true) @Composable private fun PreviewEdit() {
	MyContent(previewCfg(), null, currentTimeMillis(), PreviewConfigurable)
}
@Preview @Composable private fun PreviewInvalid() {
	MyContent(previewCfg().withStepEdited(1) {it.copy(seconds = 0)}, null, currentTimeMillis(), PreviewConfigurable)
}
@Preview(showBackground = true) @Composable private fun PreviewCounting() {
	MyContent(previewCfg(), stateCounting(), currentTimeMillis(), PreviewConfigurable)
}
@Preview(showBackground = true) @Composable private fun PreviewPaused() {
	MyContent(previewCfg(), stateCounting().copy(pauseMs = currentTimeMillis()), currentTimeMillis(), PreviewConfigurable)
}


@Composable private fun MyContent(cfg: MultistepTimerCfg, state: MultistepTimerState?, nowMs: Long, tgt: MultiTimerConfigurable) {
	CAppTheme {
		Surface(color = MaterialTheme.colors.background) {
			Column(
				verticalArrangement = Arrangement.Center,
				horizontalAlignment = Alignment.CenterHorizontally,
				modifier = Modifier
					.fillMaxSize()
					.verticalScroll(rememberScrollState()),
			) {
				if(state != null)
					Countdown(cfg, state, nowMs)
				CountingControls(cfg, state, nowMs, tgt)

				Text(
					"${cfg.steps.size} Steps - ${cfg.totalSeconds} Seconds",
					fontSize = 24.sp,
				)

				cfg.steps.forEachIndexed {i, step ->
					MyStep(i, step, cfg, state, tgt)
				}

				if(state == null) {
					Button(onClick = { tgt.replaceCfg { it.withStepAdded() } }) {
						Text("Add Step")
					}
				}
			}
		}
	}
}


@Composable private fun Countdown(cfg: MultistepTimerCfg, state: MultistepTimerState, nowMs: Long) {
	val step = cfg.steps[state.step]
	val ms = state.remainingMs(step, nowMs)
	Text("${ms / 1000}.${ms % 1000 / 100}",
		fontSize = 64.sp, fontWeight = FontWeight.Bold)
	LinearProgressIndicator(progress = 1 - (ms / step.millis.toFloat()))
	Text("#${state.step + 1}/${cfg.steps.size}",
		fontSize = 30.sp, fontWeight = FontWeight.Bold)
	Text(cfg.steps[state.step].text)
}


@Composable private fun CountingControls(cfg: MultistepTimerCfg, state: MultistepTimerState?, nowMs: Long, tgt: MultiTimerConfigurable) {
	Row(horizontalArrangement = Arrangement.Center) {
		Button(
			onClick = { tgt.btnStartStop() },
			enabled = cfg.isValid,
		) { Text(if(state == null) "Start" else "Stop") }
		Button(
			onClick = { tgt.btnPauseResume(nowMs) },
			enabled = state != null,
		) { Text(if(state?.pauseMs == null) "Pause" else "Resume") }
	}
}


@Composable private fun MyStep(i: Int, step: MultistepTimerStep, cfg: MultistepTimerCfg, state: MultistepTimerState?, tgt: MultiTimerConfigurable) {
	key(i) {
		MyStepFieldsRow(i, step, state, tgt)
		if(state == null)
			MyStepButtonsRow(i, cfg, tgt)
	}
}


@Composable private fun MyStepFieldsRow(i: Int, step: MultistepTimerStep, state: MultistepTimerState?, tgt: MultiTimerConfigurable) {
	Row(verticalAlignment = Alignment.CenterVertically) {
		Text(
			"#${i + 1}",
			fontSize = 24.sp,
			fontWeight = FontWeight.Bold,
		)
		if(state == null) {
			TextField(
				value = step.seconds.toString(),
				isError = step.seconds <= 0,
				onValueChange = { text -> onEditSeconds(i, text, tgt) },
				label = { Text("Seconds") },
				keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number, autoCorrect = false),
				modifier = Modifier.weight(0.3f, fill = false),
			)
			TextField(
				value = step.text,
				onValueChange = { text -> tgt.replaceCfg { c0 -> c0.withStepEdited(i) { s0 -> s0.copy(text = text) } } },
				label = { Text("Description") },
				modifier = Modifier.weight(1f),
			)
		} else {
			Text("${step.seconds} s : ${step.text}")
		}
	}
}


@Composable private fun MyStepButtonsRow(i: Int, cfg: MultistepTimerCfg, tgt: MultiTimerConfigurable) {
	Row {
		Button(
			enabled = i > 0,
			onClick = { tgt.replaceCfg { c0 -> c0.withStepMoved(i, i - 1) } },
		) { Text("Up") }
		Button(
			onClick = { tgt.replaceCfg { c0 -> c0.withStepRemoved(i) } },
		) { Text("Remove") }
		Button(
			enabled = i < cfg.steps.size - 1,
			onClick = { tgt.replaceCfg { c0 -> c0.withStepMoved(i, i + 1) } },
		) { Text("Down") }
	}
}
