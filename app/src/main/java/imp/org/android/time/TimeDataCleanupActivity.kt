package imp.org.android.time

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import imp.compose.CLookupAsync
import imp.org.android.AppActivity
import imp.org.android.mgmt.DataCleanupReport
import imp.org.android.mgmt.prepareDataCleanupReport
import imp.org.android.ui.CAppTheme2
import imp.org.android.ui.screens.TimeDataCleanupScreen
import imp.util.launchDefault
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope
import java.time.Instant

class TimeDataCleanupActivity : AppActivity() {
	private val screen = TimeDataCleanupScreen()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContent { Compose() }
	}

	private inner class MyHandler (
		private val uiScope: CoroutineScope,
		private val reportLoader: CLookupAsync<ReportKey, DataCleanupReport>,
	) : TimeDataCleanupScreen.Handler {
		override fun requestRegenReport(deleteBefore: Instant?) {
			reportLoader.submit(ReportKey(proposedDeleteCutoff = deleteBefore))
		}

		override fun launchDeleteProcess(deleteBefore: Instant) {
			scope.launchDefault {
				TimePeriodRepo.deleteOlderThan(deleteBefore, this@TimeDataCleanupActivity)
				withScope(uiScope) {
					screen.onFinishedDeleting()
					reportLoader.update { it!!.copy(requestedAtMs = System.currentTimeMillis()) }
				}
			}
		}
	}

	@Composable private fun Compose() = CAppTheme2 { theme ->
		val uiScope = rememberCoroutineScope()
		val reportLoader = remember { CLookupAsync<ReportKey, DataCleanupReport>(uiScope, initial = ReportKey()) { key ->
			prepareDataCleanupReport(key.proposedDeleteCutoff, this@TimeDataCleanupActivity)
		} }
		val handler = remember { MyHandler(uiScope, reportLoader) }

		screen.Compose(reportLoader.output, reportLoader.isWorking, theme, handler)
	}
}


@Immutable private data class ReportKey (
	val requestedAtMs: Long = System.currentTimeMillis(),
	val proposedDeleteCutoff: Instant? = null,
)