package imp.org.android.time

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.window.Dialog
import imp.android.impStartActivity
import imp.compose.CSurface
import imp.compose.cmsn
import imp.org.android.AppActivity
import imp.org.android.ui.AppTheme
import imp.org.android.ui.CAppTheme2
import imp.org.android.ui.screens.TimePeriodDetailScreen
import imp.org.android.ui.view.CVPastTimePeriod
import imp.org.android.ui.widgets.CImplicationExplanation
import imp.util.toUuid
import java.util.UUID

class PastTimePeriodActivity : AppActivity() {
	private val dialog = cmsn<@Composable (AppTheme) -> Unit>()
	private lateinit var periodId: UUID

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		periodId = decodeIntent()
		setContent { Render() }
	}

	@Composable private fun Render() = CAppTheme2 { theme ->
		CVPastTimePeriod(periodId, this) { period, implications, scope ->
			TimePeriodDetailScreen.Compose(period, implications, theme, handler)
			dialog.value?.invoke(theme)
		}
	}

	private val handler = object : TimePeriodDetailScreen.Handler {
		override fun onClickCurrentTimePeriod() = impStartActivity(EditCurrentTimePeriodActivity::class)

		override fun onClick(implication: PossiblyImpliedLabel) {
			dialog.value = { theme ->
				Dialog({ dialog.value = null }) {
					CSurface {
						CImplicationExplanation(implication, theme)
					}
				}
			}
		}
	}

	companion object {
		fun intent(periodId: UUID, ctx: Context) = Intent(ctx, PastTimePeriodActivity::class.java).also {
			it.putExtra("periodUuid", periodId.toString())
		}
	}

	private fun decodeIntent(): UUID {
		val str = intent.getStringExtra("periodUuid") ?: throw Exception("Missing required extra in intent: periodUuid")
		return str.toUuid()
	}
}