package imp.org.android.time

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import imp.org.android.AppActivity
import imp.org.android.hardcoded.CHardcodedReport
import imp.org.android.ui.CAppTheme2

class HardcodedReportActivity : AppActivity() {
	override fun onCreate(savedState: Bundle?) {
		super.onCreate(savedState)
		setContent { Render() }
	}

	@Composable private fun Render() = CAppTheme2 { theme ->
		CHardcodedReport(theme, this)
	}
}