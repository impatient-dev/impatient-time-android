package imp.org.android.time

import androidx.compose.runtime.Immutable
import imp.org.android.LabelI
import imp.util.require
import java.util.UUID


class LabelImplicationCycleException(val cycle: LabelImplicationCycle): Exception("The label implications form a cycle: $cycle")

/**A list of labels, where each label implies the next one in the list, and the last one implies the first.
 * If a label implies itself, the list would have size 1.*/
@Immutable class LabelImplicationCycle(val loop: List<LabelI>) {
	init {
		check(loop.isNotEmpty())
		check(loop.last().implies.any { loop.first().uuid == it.uuid })
	}

	override fun toString(): String {
		val str = StringBuilder()
		loop.forEachIndexed { i, label ->
			str.append(label.name)
			str.append(" -> ")
		}
		return str.toString()
	}
}


fun checkNoLabelImplicationCycle(labels: Collection<LabelI>) {
	val byId = labels.associateBy { it.uuid }
	val visitedIds = HashSet<UUID>()
	val chain = ArrayList<LabelI>()

	for(label in labels) {
		recursivelyThrowOnCycle(byId, visitedIds, chain, label)
	}
}

fun findLabelImplicationCycle(labels: Collection<LabelI>): LabelImplicationCycle? {
	try {
		checkNoLabelImplicationCycle(labels)
		return null
	} catch(e: LabelImplicationCycleException) {
		return e.cycle
	}
}


private fun recursivelyThrowOnCycle(
	byId: Map<UUID, LabelI>,
	visitedIds: MutableSet<UUID>,
	/**Each label in the chain implies the next one.*/
	chain: MutableList<LabelI>,
	label: LabelI,
) {
	val idx = chain.indexOfFirst { it.uuid == label.uuid }
	if(idx != -1) { // cycle detected
		val cycle = chain.subList(idx, chain.size).plus(label)
		throw LabelImplicationCycleException(LabelImplicationCycle(cycle))
	} else if(visitedIds.add(label.uuid)) { // no cycle detected, and this is a new label we haven't visited
		chain.add(label)
		for(i in label.implies) {
			val implied = byId.require(i.uuid)
			recursivelyThrowOnCycle(byId, visitedIds, chain, implied)
		}
		assert(label === chain.removeLast())
	}
}