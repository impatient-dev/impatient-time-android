package imp.org.android.time

import android.content.Context
import androidx.compose.runtime.Immutable
import imp.org.android.LabelI
import imp.org.android.LabelRepo
import imp.time.data.TimePeriodD
import imp.util.epochMsToInstant
import java.time.Instant
import java.util.UUID

/**Describes a time period that started at some point but may or may not have ended.
 * If the UUID is null, this represents a gap where we did not record anything.*/
@Immutable data class PastTimePeriodI (
	val id: Long,
	val uuid: UUID?,
	val start: Instant,
	val end: Instant?,
	val labels: List<LabelI>,
)
suspend fun TimePeriodD.toPastInfo(ctx: Context): PastTimePeriodI = PastTimePeriodI(
	id = id,
	uuid = uuid,
	start = startMs.epochMsToInstant(),
	end = endMs?.epochMsToInstant(),
	labels = labels?.map { LabelRepo.get(it, ctx) } ?: emptyList(),
)



/**Describes what time period is currently in progress (possibly none).
 * For a real saved time period, a UUID is present.
 * If there's no time period currently in progress, the UUID is null, but the start time tells you when the last actual time period ended.
 * If start is null, no time period has ever been created.*/
@Immutable data class CurrentTimePeriodI (
	val id: Long,
	val uuid: UUID?,
	val start: Instant?,
	val labels: List<LabelI>,
)