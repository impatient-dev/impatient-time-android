package imp.org.android.time

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.window.Dialog
import imp.android.impStartActivity
import imp.compose.CSurface
import imp.compose.cmsn
import imp.org.android.AppActivity
import imp.org.android.ui.AppTheme
import imp.org.android.ui.CAppTheme2
import imp.org.android.ui.screens.CurrentTimePeriodScreen
import imp.org.android.ui.view.CVCurrentTimePeriod
import imp.org.android.ui.widgets.CImplicationExplanation

class CurrentTimePeriodActivity : AppActivity() {
	private val dialog = cmsn<@Composable (AppTheme) -> Unit>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContent { Render() }
	}

	@Composable private fun Render() = CAppTheme2 { theme ->
		CVCurrentTimePeriod(this) { current, implications, scope ->
			CurrentTimePeriodScreen.Compose(current, implications, theme, handler)
			dialog.value?.invoke(theme)
		}
	}

	private val handler = object : CurrentTimePeriodScreen.Handler {
		override fun onClickCurrentTimePeriod() = impStartActivity(EditCurrentTimePeriodActivity::class)
		override fun showActivityReport() = impStartActivity(HardcodedReportActivity::class)
		override fun showHistory() = impStartActivity(RecentTimePeriodsActivity::class)

		override fun onClick(implication: PossiblyImpliedLabel) {
			dialog.value = { theme ->
				Dialog({ dialog.value = null }) {
					CSurface {
						CImplicationExplanation(implication, theme)
					}
				}
			}
		}
	}
}