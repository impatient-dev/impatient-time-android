package imp.org.android.time

import android.content.Context
import imp.json.impReadOpt
import imp.json.impWriteOrDelete
import imp.org.android.AppPaths
import imp.org.android.jackson
import imp.time.data.TimePeriodD
import imp.util.BasicCoListeners
import imp.util.SusRepo1D
import imp.util.toArrayListWithExtraCapacity
import imp.util.withReplacement
import java.time.Instant
import java.util.UUID

/**Stores time periods in a JSON file.
 * Old time periods have to be periodically cleaned up or the list will eventually get too large.*/
object TimePeriodRepo {
	suspend fun latest(ctx: Context): TimePeriodD? = wrapped.get(ctx).lastOrNull()
	suspend fun getAllEarliestFirst(ctx: Context) = wrapped.get(ctx)
	suspend fun get(id: UUID, ctx: Context): TimePeriodD = wrapped.get(ctx).find { it.uuid == id } ?: throw Exception("Time period not found: $id")

	/**Updates a time period that already exists in the repo.*/
	suspend fun update(item: TimePeriodD, ctx: Context) {
		wrapped.update(ctx) { list ->
			val i = list.indexOfLast { it.uuid == item.uuid }
			if(i == -1)
				throw Exception("There is no time period ${item.uuid} to update.")
			list.withReplacement(i, item)
		}
		_listeners.fireAsync { it.onTimePeriodChange() }
	}

	/**Ends the time period that's currently in progress (if any) and saves a new one.
	 * The end time of the previous period will be the start time of the new one.*/
	suspend fun startNew(new: TimePeriodD, ctx: Context) {
		wrapped.update(ctx) { list ->
			val prev = list.lastOrNull()
			val mut = list.toArrayListWithExtraCapacity(1)
			if(prev != null && prev.endMs == null)
				mut.set(mut.lastIndex, prev.copy(endMs = new.startMs))
			mut.add(new)
			mut
		}
		_listeners.fireAsync { it.onTimePeriodChange() }
	}

	fun isOlderThan(period: TimePeriodD, cutoff: Instant): Boolean = (period.endMs ?: period.startMs) < cutoff.toEpochMilli()

	suspend fun countOlderThan(cutoff: Instant, ctx: Context): Long = wrapped.get(ctx)
		.count { isOlderThan(it, cutoff) }.toLong()

	suspend fun deleteOlderThan(cutoff: Instant, ctx: Context) {
		wrapped.update(ctx) { list -> list.filter { !isOlderThan(it, cutoff) } }
		_listeners.fireAsync { it.onTimePeriodChange() }
	}


	interface Listener {
		/**Called when any time period is created or updated.*/
		suspend fun onTimePeriodChange()
	}
	private val _listeners = BasicCoListeners<Listener>()
	val listeners get() = _listeners.toInterface


	private object wrapped : SusRepo1D<List<TimePeriodD>, Context>() {
		private fun path(ctx: Context) = AppPaths.timePeriodsJson(ctx).toPath()

		override suspend fun load(d: Context): List<TimePeriodD> =
			jackson.impReadOpt<List<TimePeriodD>>(path(d)) ?: emptyList()

		override suspend fun save(item: List<TimePeriodD>, d: Context) {
			jackson.impWriteOrDelete(path(d), item)
		}
	}
}