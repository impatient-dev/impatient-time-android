package imp.org.android.time

import imp.org.android.LabelI
import imp.util.abbreviateEllipsis
import imp.util.mapToArrayList
import imp.util.putNew
import imp.util.require
import java.util.UUID

/**A graph (a set of connected nodes) that calculates what labels are implied and not-implied based on a set of directly applied labels.
 * The constructor does all the calculating; afterward, you just need to retrieve the results summary.*/
class LabelImplicationGraph (
	private val direct: List<LabelI>,
	private val labelsById: Map<UUID, LabelI>,
	sortForUi: Boolean = false
) {
	private val directNodes: List<LabelImplicationNode> = direct.map { LabelImplicationNode(it) }
	private val nodesById = HashMap<UUID, LabelImplicationNode>()
	private val exclusiveSetsById = HashMap<UUID, LabelImplicationNode>()

	private val foundConflictsWithDirect = ArrayList<ImpliedConflictLabel>()
	private val foundConflictsWithImplied = ArrayList<ImpliedConflictLabel>()

	val summary: ImpliedLabels

	init {
		for(node in this.directNodes)
			nodesById.putNew(node.label.uuid, node) { "duplicate: $node" }
		for(node in this.directNodes)
			recursivelyFillOutTree(node)
		calculateDepths(firstTime = true)
		handleConflicts()
		calculateDepths()

		val impliedList = nodesById.values.asSequence()
			.filter { it.impliedBy.isNotEmpty() } // exclude directly applied
			.map { ImpliedLabel(it.toChain()) }
			.toMutableList()
		if(sortForUi)
			impliedList.sortBy { it.label.name }

		val all = HashMap<UUID, PossiblyImpliedLabel>(direct.size + impliedList.size + foundConflictsWithDirect.size + foundConflictsWithImplied.size)
		direct.forEach { all.putNew(it.uuid, DirectlyAppliedLabel(it)) { _ -> "duplicate direct label: $it" } }
		impliedList.forEach { all.putNew(it.label.uuid, it) { _ -> "duplicate implied label: $it" } }
		foundConflictsWithDirect.forEach { all.putNew(it.label.uuid, it) { _ -> "duplicate conflict with direct: $it" } }
		foundConflictsWithImplied.forEach { all.putNew(it.label.uuid, it) { _ -> "duplicate conflict with implied: $it" } }

		summary = ImpliedLabels(
			impliedById = all,
			direct = direct,
			implied = impliedList,
			conflictsWithDirect = foundConflictsWithDirect,
			conflictsWithImplied = foundConflictsWithImplied,
		)
	}

	/**Adds implied  nodes to the tree, and implies and implied-by relationships between nodes.
	 * Adds any exclusive sets to the map.*/
	private fun recursivelyFillOutTree(node: LabelImplicationNode) {
		if(node.label.exclusiveSet)
			exclusiveSetsById.put(node.label.uuid, node)
		for(imp in node.label.implies) {
			val impliedLabel = labelsById.require(imp.uuid)
			val impliedNode = nodesById.getOrPut(impliedLabel.uuid) { LabelImplicationNode(impliedLabel) }
			node.implies.add(impliedNode)
			if(impliedNode.impliedBy.add(node))
				recursivelyFillOutTree(impliedNode)
		}
	}

	fun calculateDepths(firstTime: Boolean = false) {
		if(!firstTime)
			for(node in directNodes)
				node.recursivelySetDepthTo(-1)
		for(node in directNodes)
			node.recursivelyCalculateDepth()
	}

	private fun remove(node: LabelImplicationNode) {
		node.impliedBy.forEach { it.implies.remove(node) }
		node.implies.forEach { it.impliedBy.remove(node) }
		nodesById.remove(node.label.uuid)
	}

	/**For any cases where multiple nodes in the same exclusive set are part of the graph,
	 * removes implied (but not directly applied) nodes in the set to resolve the conflict.
	 * This function relies on node depths being correct.*/
	private fun handleConflicts() {
		for(set in exclusiveSetsById.values) {
			if(set.impliedBy.size <= 1)
				continue

			val chains = set.impliedBy.mapToArrayList { it.toChain() }
			chains.sortBy { it.size }
			val conflict = LabelImplicationConflict(set.label, chainsShortestFirst = chains)

			val addConflictsTo = if(conflict.isAnyDirectlyApplied()) foundConflictsWithDirect else foundConflictsWithImplied
			for(n in set.impliedBy)
				if(n.impliedBy.isNotEmpty()) // skip directly applied labels
					addConflictsTo.add(ImpliedConflictLabel(n.label, conflict))

			for(n in set.impliedBy.toList()) // copy to avoid concurrent modification
				if(n.impliedBy.isNotEmpty()) // if implied instead of directly implied
					remove(n)
		}
		exclusiveSetsById.clear()
	}
}




private class LabelImplicationNode (
	val label: LabelI,
	/**How many implications it took to reach this node via the shortest path.
	 * 0 for directly applied labels; higher for implied labels; -1 initially before depth has been calculated.*/
	var depth: Int = -1,
) {
	val impliedBy = HashSet<LabelImplicationNode>()
	val implies = HashSet<LabelImplicationNode>()
	override fun toString() = "LINode(label ${label.uuid} ${label.name.abbreviateEllipsis(80)} depth=$depth)"
}


/**Sets depth to the provided value for this node any any other node directly or indirectly implied.*/
private fun LabelImplicationNode.recursivelySetDepthTo(depth: Int, stopIfDepthMatches: Boolean = false) {
	if(stopIfDepthMatches && this.depth == depth)
		return
	this.depth = depth
	for(node in implies)
		node.recursivelySetDepthTo(depth)
}

/**Recursively calculates the depth of this node and all nodes it implies.
 * Stops if the depth it would set is higher than the depth already assigned to this node (which indicates we've found a longer path to the same node).
 * However, if a node has a negative depth, this is not considered a valid depth and is replaced by the depth argument.*/
private fun LabelImplicationNode.recursivelyCalculateDepth(depth: Int = 0) {
	if(this.depth >= 0 && this.depth > depth)
		return
	this.depth = depth
	for(node in implies)
		node.recursivelyCalculateDepth(depth + 1)
}

/**Returns one of the shortest paths to this node, starting with a directly applied node and ending with this one.
 * This function relies on all node depths being correct, and assumes that there is a path (i.e. this node isn't disconnected or removed from the graph).*/
private fun LabelImplicationNode.toChain(): List<LabelI> {
	val list = ArrayList<LabelI>(depth)
	var node = this
	while(true) {
		list.add(node.label)
		if(node.depth == 0)
			break
		node = node.impliedBy.find { it.depth == node.depth - 1 }!!
	}
	list.reverse()
	assert(list.size == this.depth + 1) { "produced a length=${list.size} chain (expected ${depth + 1}) but node depth is $depth" }
	return list
}
