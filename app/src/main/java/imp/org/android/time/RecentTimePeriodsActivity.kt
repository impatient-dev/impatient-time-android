package imp.org.android.time

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import imp.compose.view.CVHelper
import imp.org.android.AppActivity
import imp.org.android.ui.CAppTheme2
import imp.org.android.ui.screens.RecentTimePeriodsScreen
import java.time.Instant

/**Shows past time periods.*/
class RecentTimePeriodsActivity : AppActivity() {
	override fun onCreate(savedState: Bundle?) {
		super.onCreate(savedState)
		setContent { Render() }
	}

	private suspend fun defaultLoad(): List<PastTimePeriodI> {
		val data = TimePeriodRepo.getAllEarliestFirst(this).asReversed()
		val info = ArrayList<PastTimePeriodI>(data.size)
		for(period in data)
			info.add(period.toPastInfo(this))
		return info
	}

	@Composable private fun Render() = CAppTheme2 { theme ->
		CVHelper(defaultLoad = { defaultLoad() }) { data, loading, scope, reload ->
			RecentTimePeriodsScreen.Compose(
				periods = data,
				endOfList = true,
				theme = theme,
				handler = handler,
				now = Instant.now(),
			)
		}
	}

	private val handler = object : RecentTimePeriodsScreen.Handler {
		override fun loadMore() {
			throw UnsupportedOperationException() // currently, all time periods are loaded at once
		}

		override fun onSelect(period: PastTimePeriodI) {
			if(period.uuid != null)
				startActivity(PastTimePeriodActivity.intent(period.uuid, this@RecentTimePeriodsActivity))
		}
	}
}