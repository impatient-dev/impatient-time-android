package imp.org.android.time

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import imp.android.compose.withMain
import imp.compose.CSpinnerHC
import imp.compose.CSurfaceFull
import imp.org.android.AppActivity
import imp.org.android.LabelI
import imp.org.android.ui.CAppTheme2
import imp.org.android.ui.screens.CurrentTimePeriodScreen
import imp.org.android.ui.screens.EditCurrentTimePeriodScreen
import imp.org.android.ui.view.CVAllLabels
import imp.org.android.ui.view.CVCurrentTimePeriod
import imp.time.data.TimePeriodD
import imp.util.launchDefault
import java.time.Instant
import java.util.UUID

class EditCurrentTimePeriodActivity : AppActivity() {
	private val screenOpenTime = Instant.now()

	override fun onCreate(savedState: Bundle?) {
		super.onCreate(savedState)
		setContent { Render() }
	}

	@Composable private fun Render() {
		CAppTheme2 { theme ->
			CVCurrentTimePeriod(this, skipImplications = true) { current, _, _ ->
				CVAllLabels(this) { allLabels ->
					if(current == null || allLabels == null)
						CSpinnerHC()
					else
						EditCurrentTimePeriodScreen.Compose(current, allLabels, theme, handler)
				}
			}
		}
	}

	private val handler = object : EditCurrentTimePeriodScreen.Handler {
		override fun saveTimePeriod(new: Boolean, startAtSubmit: Boolean, labels: List<LabelI>) {
			val time = if(startAtSubmit) Instant.now() else screenOpenTime
			scope.launchDefault {
				val labelUuids = labels.map { it.uuid }
				val period = TimePeriodD(-1, UUID.randomUUID(), startMs = time.toEpochMilli(), endMs = null, description = null, labels = labelUuids)
				TimePeriodRepo.startNew(period, this@EditCurrentTimePeriodActivity)
				withMain { finish() }
			}
		}
	}
}