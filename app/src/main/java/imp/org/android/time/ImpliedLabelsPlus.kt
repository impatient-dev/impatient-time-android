//package imp.org.android.time
//
//import androidx.compose.runtime.Immutable
//
///**Contains extra info about which labels are implied, for use by the UI.*/
//@Immutable class ImpliedLabelsPlus (
//	val summary: ImpliedLabels,
//) {
//	val notImpliedDueToConflict: List<ImpliedConflictLabel> = summary.conflicts.asSequence()
//		.flatMap { it.chainsShortestFirst }
//		.filter { it.size > 1 } // exclude directly applied
//		.map { ICL}
//}