package imp.org.android.time

import androidx.compose.runtime.Immutable
import imp.org.android.LabelI
import imp.time.data.LabelD
import imp.util.abbreviateEllipsis
import java.util.UUID

/**Contains a list of directly applied labels, plus information about which labels are and are not implied by these labels.*/
@Immutable class ImpliedLabels (
	private val impliedById: Map<UUID, PossiblyImpliedLabel>,
	/**The labels that apply directly, without looking at any "implies" relationships.*/
	val direct: List<LabelI>,
	/**The labels that are implied by the direct ones, excluding labels that cannot be implied due to conflicts.*/
	val implied: List<ImpliedLabel>,
	/**Labels not implied because they would conflict with a directly applied label.*/
	val conflictsWithDirect: List<ImpliedConflictLabel>,
	/**Labels not implied because they would conflict with an implied label.*/
	val conflictsWithImplied: List<ImpliedConflictLabel>,
) {
	operator fun get(label: LabelD): PossiblyImpliedLabel? = impliedById[label.uuid]
	operator fun get(label: LabelI): PossiblyImpliedLabel? = impliedById[label.uuid]
	fun getByLabelId(id: UUID): PossiblyImpliedLabel? = impliedById[id]
	fun implies(label: LabelD) = this[label]?.isImplied == true
	fun implies(label: LabelI) = this[label]?.isImplied == true
	fun impliesLabelById(labelId: UUID): Boolean = getByLabelId(labelId)?.isImplied == true
	fun impliesAnyLabelById(ids: Collection<UUID>): Boolean = ids.any { impliesLabelById(it) }
}


/**A label that is either implied or not-implied by some other set of labels.*/
sealed interface PossiblyImpliedLabel {
	/**The label that is implied or not-implied by the direct labels.*/
	val label: LabelI
	val isImplied: Boolean
}

@Immutable class DirectlyAppliedLabel (
	override val label: LabelI,
) : PossiblyImpliedLabel {
	override val isImplied = true
	override fun toString() = "DirectlyAppliedLabel(${label.uuid} ${label.name.abbreviateEllipsis(80)})"
}

/**This is just a list of labels, where each label implies the next label in the list.
 * The first label is a label that was directly applied; the last label is one that it directly or indirectly implies.*/
@Immutable class ImpliedLabel (
	val chain: List<LabelI>,
) : PossiblyImpliedLabel {
	init {
		check(chain.size >= 2)
	}
	override val isImplied = true
	override val label get() = chain.last()
	/**The first label in the chain that eventually implied [label].
	 * The root label is one of the "direct"-ly applied labels.*/
	inline val root get() = chain.first()
	override fun toString() = "ImpliedLabel(chainLength=${chain.size} ${label.uuid} ${label.name.abbreviateEllipsis(80)})"
}

/**A label that is an exclusive set, and two or members that conflict - both are implied and/or directly applied.
 * Each chain is a List of labels, and gives the path that would have implied the label, working the same way as the chain in [ImpliedLabel].
 * There must be at least 2 chains in the list.*/
@Immutable class LabelImplicationConflict (
	val set: LabelI,
	val chainsShortestFirst: List<List<LabelI>>,
) {
	init {
		check(chainsShortestFirst.size >= 2)
	}

	fun isAnyDirectlyApplied() = chainsShortestFirst[0].size == 1
}

/**A label that would've been implied, but isn't because it's a member of an exclusive set and some other member of the set was also applied or implied.*/
@Immutable class ImpliedConflictLabel(
	override val label: LabelI,
	val conflict: LabelImplicationConflict,
) : PossiblyImpliedLabel {
	override val isImplied = false
	override fun toString() = "ImpliedConflictLabel(${label.uuid} ${label.name.abbreviateEllipsis(80)})"

	fun mostDirectConflictingChain(): List<LabelI> {
		var ret = conflict.chainsShortestFirst[0]
		if(ret.last().uuid == label.uuid)
			ret = conflict.chainsShortestFirst[1]
		return ret
	}

	/**Returns whether any of the conflicting label(s) were directly applied, instead of implied by other labels.*/
	fun conflictsWithDirectlyApplied(): Boolean = mostDirectConflictingChain().size == 1
}