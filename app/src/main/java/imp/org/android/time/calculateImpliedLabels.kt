package imp.org.android.time

import android.content.Context
import imp.org.android.LabelI
import imp.org.android.LabelRepo
import imp.util.require
import java.util.UUID


suspend fun calculateImpliedLabelsFromIds(directIds: Collection<UUID>, ctx: Context, sortForUi: Boolean = false): ImpliedLabels {
	val allById = LabelRepo.getAllByUuid(ctx)
	val direct = directIds.map { allById.require(it) }
	return calculateImpliedLabels(direct, allById, sortForUi = sortForUi)
}

suspend fun calculateImpliedLabels(direct: List<LabelI>, ctx: Context, sortForUi: Boolean = false): ImpliedLabels
	= calculateImpliedLabels(direct, LabelRepo.getAllByUuid(ctx), sortForUi)



private fun calculateImpliedLabels(direct: List<LabelI>, allById: Map<UUID, LabelI>, sortForUi: Boolean = false): ImpliedLabels {
	val graph = LabelImplicationGraph(direct, allById, sortForUi = sortForUi)
	return graph.summary
}
