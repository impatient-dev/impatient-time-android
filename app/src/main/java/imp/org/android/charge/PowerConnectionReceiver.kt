package imp.org.android.charge

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/**This class is notified when the charging cable is plugged in or removed.*/
class PowerConnectionReceiver : BroadcastReceiver() {
	override fun onReceive(ctx: Context, intent: Intent) {
		ChargeBackgroundWorker.onChargingCableChange(ctx)
	}
}