package imp.org.android.charge

import android.content.Context
import androidx.compose.runtime.Immutable
import com.fasterxml.jackson.module.kotlin.readValue
import imp.json.impWrite
import imp.org.android.AppPaths
import imp.org.android.jackson
import imp.util.logger
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.nio.file.Files


/**What warning (if any) we're currently providing to the user about the battery charge.*/
@Immutable data class ChargeNotifyState (
	/**What we're currently warning the user about, if anything.*/
	val warning: HiLo?,
	/**How many times in a row we've warned the user about the battery situation.*/
	val consecutive: Int,
) {
	enum class HiLo {
		LO,
		HI,
	}
}


object ChargeNotifyRepo {
	private val log = ChargeNotifyRepo::class.logger
	private val mutex = Mutex()
	/**Null when not loaded.*/
	private var status: ChargeNotifyState? = null

	suspend fun get(ctx: Context): ChargeNotifyState = mutex.withLock {
		if(status == null) {
			val f = AppPaths.chargeNotificationJson(ctx)
			if(f.exists()) {
				try {
					status = jackson.readValue<ChargeNotifyState>(f)
				} catch(e: Exception) {
					log.error("Failed to load charge notification state.", e)
					status = ChargeNotifyState(null, 0)
				}
			}
		}
		return status!!
	}

	suspend fun set(s: ChargeNotifyState, ctx: Context): Unit = mutex.withLock {
		val f = AppPaths.chargeNotificationJson(ctx).toPath()
		if(s.warning == null && s.consecutive == 0)
			Files.deleteIfExists(f)
		else
			jackson.impWrite(f, s)
		status = s
	}
}