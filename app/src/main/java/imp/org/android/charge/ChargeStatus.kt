package imp.org.android.charge

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import androidx.compose.runtime.Immutable
import imp.util.logger

private val log = ChargeStatus::class.logger


/**Returns null if we fail to read the level for some reason.
 * This should probably only be called on the main thread.*/
fun Context.currentChargeStatus(): ChargeStatus? {
	try {
		val status = registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))!!
		val level = status.getIntExtra(BatteryManager.EXTRA_LEVEL, -1).also { check(it != -1) { "No battery level info" } }
		val scale = status.getIntExtra(BatteryManager.EXTRA_SCALE, -1).also { check(it != -1) { "No battery scale info" } }
		val charging = when(status.getIntExtra(BatteryManager.EXTRA_STATUS, -1)) {
			BatteryManager.BATTERY_STATUS_CHARGING -> true
			BatteryManager.BATTERY_STATUS_FULL -> true
			-1 -> throw Exception("No charging info")
			else -> false
		}
		return ChargeStatus(level.toDouble() / scale, charging)
	} catch(e: Exception) {
		log.error("Unable to get battery level.", e)
		return null
	}
}


@Immutable data class ChargeStatus (
	/**A number 0-1.*/
	val level: Double,
	/**True if we're plugged in and charging.*/
	val isCharging: Boolean,
)