package imp.org.android.charge

import android.os.Bundle
import androidx.activity.compose.setContent
import imp.org.android.AppActivity
import imp.org.android.ui.CAppTheme2
import imp.android.compose.longToast
import imp.android.compose.withMain
import imp.util.launchDefault
import imp.util.launchIo
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**Lets the user configure notifications when the battery gets too high or low.*/
class ChargeActivity : AppActivity() {
	private val ui = ChargeComposition(null, null, null)
	private var pollJob: Job? = null

	override fun onCreate(savedState: Bundle?) {
		super.onCreate(savedState)
		setContent {
			CAppTheme2 { theme -> ui.Compose(theme, handler) }
		}
		scope.launchIo {
			val s = ChargeSettingsRepo.get(this@ChargeActivity)
			withMain {
				ui.max.value = s.max
				ui.min.value = s.min
			}
		}
	}

	override fun onStart() {
		super.onStart()
		pollJob = scope.launch { pollBatteryOnMain() }
		scope.launchIo { backgroundLoad() }
	}

	override fun onStop() {
		super.onStop()
		pollJob?.cancel()
		pollJob = null
	}

	private suspend fun backgroundLoad() {
		val settings = ChargeSettingsRepo.get(this)
		withMain {
			ui.max.value = settings.max
			ui.min.value = settings.min
		}
	}

	private suspend fun pollBatteryOnMain() {
		while(true) {
			val charge = currentChargeStatus()
			ui.status.value = charge
			if(charge == null)
				longToast("Unable to check battery status.")
			delay(30_000)
		}
	}

	private inline fun updateSettings(transform: (ChargeSettings) -> ChargeSettings) {
		val max = ui.max.value ?: return
		val min = ui.min.value ?: return
		val settings = transform(ChargeSettings(max = max, min = min))
		ui.max.value = settings.max
		ui.min.value = settings.min
		scope.launchDefault {
			ChargeSettingsRepo.set(settings, this@ChargeActivity)
			if(settings.max.enabled || settings.min.enabled)
				ChargeBackgroundWorker.onSettingsEdit(this@ChargeActivity)
		}
	}

	private val handler = object : ChargeComposition.Handler {
		override fun setMaxNotify(limit: ChargeLimit) = updateSettings { it.copy(max = limit) }
		override fun setMinNotify(limit: ChargeLimit) = updateSettings { it.copy(min = limit) }
	}
}