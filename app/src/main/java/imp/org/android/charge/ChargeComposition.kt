package imp.org.android.charge

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.tooling.preview.Preview
import imp.compose.*
import imp.compose.style.CSText
import imp.org.android.ui.AppTheme
import imp.org.android.ui.CAppTheme2
import imp.util.exToNull
import java.lang.Integer.parseInt
import kotlin.math.roundToInt

private val HI_PERCENTAGES = listOf(60, 65, 70, 75, 80, 85, 90, 95, 100)
private val LO_PERCENTAGES = listOf(15, 20, 25, 30, 35)


class ChargeComposition (
	status: ChargeStatus?,
	max: ChargeLimit?,
	min: ChargeLimit?,
) {
	val status = cms(status)
	/**Null when not loaded.*/
	val max = cms(max)
	/**Null when not loaded.*/
	val min = cms(min)

	private val dialog = cmsn<Composition>()

	@Composable fun Compose(theme: AppTheme, handler: Handler?) = CSurface {
		CColMaxScrollV {
			val s = status.value
			if(s != null) {
				val hi = max.value
				val lo = min.value
				dialog.value?.Compose()
				theme.titleSmall.CW("Battery")
				CCurrentCharge(theme, s, hi, lo)
				CNotifyControl(theme, hi, HI_PERCENTAGES, "Max Charge", "Notify me when the battery charges past this percentage.") { handler?.setMaxNotify(it) }
				CNotifyControl(theme, lo, LO_PERCENTAGES, "Min Charge", "Notify me when the battery gets below this percentage.") { handler?.setMinNotify(it) }
			}
		}
	}

	@Composable private fun CCurrentCharge(theme: AppTheme, status: ChargeStatus?, max: ChargeLimit?, min: ChargeLimit?) {
		if(status != null) {
			val level = status.level
			val lo = min?.level ?: 0.1
			val hi = max?.level ?: Double.POSITIVE_INFINITY
			val style: CSText = when {
				level > 1.0 -> theme.hugeBad
				level > hi -> theme.hugeGood
				level < lo -> theme.hugeBad
				else -> theme.huge
			}
			style.C("${(level * 100).roundToInt()}%")
			if(status.isCharging)
				theme.strongGood.C(if(level > hi) "Adequately Charged" else "Charging")
		}
	}

	@Composable private fun CNotifyControl(
		theme: AppTheme,
		limit: ChargeLimit?,
		percentageOptions: List<Int>,
		title: String,
		description: String,
		change: (ChargeLimit) -> Unit,
	) {
		if(limit != null) {
			val str = remember { cms(limit.level?.let { (it * 100).roundToInt().toString() } ?: "")}
			fun setStr(it: String) {
				str.value = it
				val parsed = exToNull {
					val i = parseInt(it)
					check(i > 0 && i <= 100)
					i / 100.0
				}
				change(limit.copy(level = parsed))
			}

			CCheckWCol(checked = limit.enabled, enabled = limit.enabled || limit.level != null, setChecked = { change(limit.copy(enabled = it)) }) {
				theme.strong.C(title)
				theme.body.C(description)
			}
			CRowWL {
				CTextField(str.value, "Percentage", "50", onValueChange = ::setStr)
				theme.btn.C("...") {
					dialog.value = ChoosePercentageDialog(theme, percentageOptions, close = { dialog.value = null }, choose = { setStr(it.toString()) })
				}
			}
		}
	}



	interface Handler {
		fun setMaxNotify(limit: ChargeLimit)
		fun setMinNotify(limit: ChargeLimit)
	}
}



@Preview @Composable private fun Preview_Basic() = CAppTheme2 { theme -> ChargeComposition(
	ChargeStatus(1.0, false), max = ChargeLimit(null, false), min = ChargeLimit(null, false),
).Compose(theme, null) }

@Preview @Composable private fun Preview_Half() = CAppTheme2 { theme -> ChargeComposition(
	ChargeStatus(0.7, false), max = ChargeLimit(0.6, true), min = ChargeLimit(0.2, false),
).Compose(theme, null) }

@Preview @Composable private fun Preview_Charging_Low() = CAppTheme2 { theme -> ChargeComposition(
	ChargeStatus(0.1, true), max = ChargeLimit(0.6, true), min = ChargeLimit(0.15, true),
).Compose(theme, null) }