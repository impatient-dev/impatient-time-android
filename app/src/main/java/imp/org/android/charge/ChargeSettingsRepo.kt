package imp.org.android.charge

import android.content.Context
import androidx.compose.runtime.Immutable
import com.fasterxml.jackson.module.kotlin.readValue
import imp.json.impWrite
import imp.org.android.AppPaths
import imp.org.android.jackson
import imp.util.logger
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.nio.file.Files

/**Settings for when to notify the user about the battery getting too high/low.*/
@Immutable data class ChargeSettings (
	val max: ChargeLimit,
	val min: ChargeLimit,
) {
	/**Returns false if any settings are invalid, or if no notifications are enabled.*/
	val validAndEnabled: Boolean get() = max.isValid && min.isValid && (max.enabled || min.enabled)
}

@Immutable data class ChargeLimit (
	/**How high or low the charge is allowed to get, 0-1. Shouldn't't be null if this limit is enabled.*/
	val level: Double?,
	/**Whether to notify the user.*/
	val enabled: Boolean,
) {
	val isValid: Boolean get() = !enabled || level != null
}



object ChargeSettingsRepo {
	private val log = ChargeSettingsRepo::class.logger
	private val mutex = Mutex()
	private var settings: ChargeSettings? = null

	suspend fun get(ctx: Context): ChargeSettings = mutex.withLock {
		if(settings == null) {
			try {
				settings = jackson.readValue<ChargeSettings>(AppPaths.chargeLimitsJson(ctx))
			} catch(e: Exception) {
				log.debug("Failed to load charge limit settings.", e)
				settings = ChargeSettings(ChargeLimit(null, false), ChargeLimit(null, false))
			}
		}
		return settings!!
	}

	suspend fun set(settings: ChargeSettings, ctx: Context) = mutex.withLock {
		val f = AppPaths.chargeLimitsJson(ctx).toPath()
		if(settings.max.level == null && settings.min.level == null)
			Files.deleteIfExists(f)
		else
			jackson.impWrite(f, settings)
		this.settings = settings
	}
}