package imp.org.android.charge

import androidx.compose.runtime.Composable
import androidx.compose.ui.window.Dialog
import imp.compose.CCheckW
import imp.compose.CColC
import imp.compose.CSurface
import imp.compose.Composition
import imp.org.android.ui.AppTheme

/**Lets the user choose a percentage from a list.*/
class ChoosePercentageDialog (
	private val theme: AppTheme,
	private val percentages: List<Int>,
	private val close: () -> Unit,
	private val choose: (Int) -> Unit,
) : Composition {
	@Composable override fun Compose() = Dialog(close) {
		theme.dialog.CSurface {
			CColC {
				for(p in percentages)
					theme.body.CCheckW("$p%", false) { choose(p); close() }
			}
		}
	}
}