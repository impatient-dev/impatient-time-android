package imp.org.android.charge

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.AudioAttributes
import androidx.work.*
import imp.org.android.NotificationIds
import imp.org.android.R
import imp.util.*
import imp.android.compose.notificationManager
import imp.android.compose.pendingIntentForActivity
import imp.android.compose.resourceUri
import imp.android.compose.withMain
import imp.util.fmt.Millis
import java.time.Duration
import kotlin.math.abs

private val log = ChargeBackgroundWorker::class.logger
private const val WORK_NAME = "monitor_charge"
private const val CHANNEL_ID = "battery"


class ChargeBackgroundWorker(appContext: Context, workerParams: WorkerParameters) : CoroutineWorker(appContext, workerParams) {
	override suspend fun doWork(): Result {
		try {
			performCheck(applicationContext)
			return Result.success()
		} catch (e: Exception) {
			log.error("Error in background battery checker.", e)
			return Result.retry()
		}
	}

	companion object {
		/**Called when the user edits the settings for when to notify about high/low battery.*/
		fun onSettingsEdit(ctx: Context) {
			scheduleNextCheck(ctx, DELAY_AFTER_UI_EDIT)
		}
		/**Called when the charging cable is plugged in or removed.*/
		fun onChargingCableChange(ctx: Context) {
			scheduleNextCheck(ctx, DELAY_AFTER_CHARGING_CABLE_CHANGE)
		}
	}
}


/**Launches a background task that periodically checks the battery level and notifies the user if it goes over/under the limits.
 * The work is immediately cancelled if at any point the current settings say we aren't supposed to notify the user for anything.*/
private fun scheduleNextCheck(
	ctx: Context,
	/**How long to wait until checking the charge level (and possibly notifying about it).
	 * The default value is intended for use by the UI - if the user just looked at our UI, he's surely aware of the battery state and doesn't need to be notified for a while.*/
	delay: Duration = DELAY_AFTER_UI_EDIT,
) {
	val work = OneTimeWorkRequestBuilder<ChargeBackgroundWorker>()
		.setInitialDelay(DELAY_AFTER_UI_EDIT)
		.build()
	WorkManager.getInstance(ctx)
		.enqueueUniqueWork(WORK_NAME, ExistingWorkPolicy.REPLACE, work)
}


private suspend fun performCheck(ctx: Context) {
	val settings = ChargeSettingsRepo.get(ctx)
	if(!settings.validAndEnabled)
		return
	val charge = withMain { ctx.currentChargeStatus() } ?: throw Exception("Unable to fetch battery info")

	val warning = if(charge.isCharging) {
		if(settings.max.enabled && charge.level > settings.max.level!!) ChargeNotifyState.HiLo.HI else null
	} else {
		if(settings.min.enabled && charge.level < settings.min.level!!) ChargeNotifyState.HiLo.LO else null
	}

	if(warning != null) {
		notifyAboutWarning(warning, settings, ctx)
	}

	val prevStatus = ChargeNotifyRepo.get(ctx)
	val newStatus = updatedStatus(prevStatus, warning)
	ChargeNotifyRepo.set(newStatus, ctx)

	if(prevStatus.warning != null && warning == null)
		ctx.notificationManager.cancel(NotificationIds.BATTERY)
	val delay = if(warning == null) delayUntilNextCheckOk(charge, settings) else delayAfterConsecutiveWarnings(newStatus.consecutive)
	scheduleNextCheck(ctx, delay)
}


private fun notifyAboutWarning(warning: ChargeNotifyState.HiLo, settings: ChargeSettings, ctx: Context) {
	val channel = NotificationChannel(CHANNEL_ID, "Battery", NotificationManager.IMPORTANCE_DEFAULT)
	channel.description = "Notifications about the battery getting too full/empty, if you've enabled these notifications."
	channel.setSound(ctx.resourceUri(R.raw.battery_notification), AudioAttributes.Builder().build())
	val mgr = ctx.notificationManager
	mgr.createNotificationChannel(channel)
	val hi = warning == ChargeNotifyState.HiLo.HI
	val note = Notification.Builder(ctx, channel.id)
		.setContentTitle(if(hi) "Battery Is Charged" else "Battery Is Low")
		.setContentText(if(hi) "Charged to at least ${(settings.max.level!! * 100).toInt()}%" else "Charge lower than ${(settings.min.level!! * 100).toInt()}%")
		.setSmallIcon(if(hi) R.drawable.battery_check else R.drawable.battery_alert)
		.setContentIntent(ctx.pendingIntentForActivity(ChargeActivity::class))
		.setOnlyAlertOnce(false)
		.build()
	mgr.notify(NotificationIds.BATTERY, note)
}

private fun updatedStatus(prev: ChargeNotifyState, warning: ChargeNotifyState.HiLo?): ChargeNotifyState {
	if(warning == null)
		return if(prev.warning == null && prev.consecutive == 0) prev else ChargeNotifyState(null, 0)
	if(warning == prev.warning)
		return prev.copy(consecutive = prev.consecutive + 1)
	return ChargeNotifyState(warning, 1)
}


private val DELAY_AFTER_UI_EDIT = Duration.ofMinutes(10)
private val DELAY_AFTER_CHARGING_CABLE_CHANGE = Duration.ofMinutes(10)
private val DELAY_OK_MAX = Duration.ofMinutes(15)
private val DELAY_OK_MIN = Duration.ofMinutes(1)
/**The minimum time it might take to (dis)charge the entire battery.*/
private const val WHOLE_BATTERY_MS = 1 * Millis.HOUR

/**Returns how long to wait before repeating a notification.*/
private fun delayAfterConsecutiveWarnings(i: Int): Duration = when(i) {
	0 -> unreachable()
	1 -> Duration.ofMinutes(10)
	2 -> Duration.ofMinutes(20)
	3 -> Duration.ofMinutes(30)
	else -> Duration.ofHours(1)
}

/**Returns how long to wait until the next check if we're not currently warning the user about anything.*/
private fun delayUntilNextCheckOk(charge: ChargeStatus, settings: ChargeSettings): Duration {
	val limit = if(charge.isCharging) settings.max else settings.min
	if(!limit.enabled)
		return DELAY_OK_MAX
	val diff = abs(limit.level!! - charge.level)
	val expectedMs = (diff * WHOLE_BATTERY_MS).toLong()
	return clamp(DELAY_OK_MIN, Duration.ofMillis(expectedMs), DELAY_OK_MAX)
}