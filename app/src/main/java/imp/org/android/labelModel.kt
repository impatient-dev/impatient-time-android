package imp.org.android

import imp.time.data.LabelD
import imp.util.abbreviateEllipsis
import imp.util.bitwiseAnd
import java.util.UUID

data class LabelI(
	val id: Long,
	val uuid: UUID,
	val name: String,
	val description: String?,
	val flags: Int,
	val implies: Set<LabelD>,
	val impliedBy: Set<LabelD>,
) {
	override fun toString() = "LabelI($uuid ${name.abbreviateEllipsis(80)})"

	enum class Flag(val bit: Int) {
		/**This label shouldn't be directly assigned to time periods. (Used for labels that just group other labels.)*/
		notDirect(1),
		/**Direct children (labels that imply this label) are mutually exclusive.*/
		exclusiveSet(1 shl 1),
//		setIsOrdered(1 shl 2),
//		doNotDisturb(1 shl 3),
	}

	operator fun get(flag: Flag): Boolean = flags.bitwiseAnd(flag.bit)
	inline val notDirect get() = this[Flag.notDirect]
	inline val exclusiveSet get() = this[Flag.exclusiveSet]

	fun isMemberOfExclusiveSet() = implies.any { it[Flag.exclusiveSet] }
}

operator fun LabelD.get(flag: LabelI.Flag): Boolean = flag.bit.bitwiseAnd(this.flags)