package imp.org.android.track

import androidx.compose.runtime.Immutable
import imp.org.android.labelLoader
import imp.time.*
import imp.time.data.loader.LabelE
import imp.time.data.loader.TimePeriodE
import imp.time.data.loader.TimePeriodSampleE

//TODO rm

@Immutable data class LabelRefI (
	val id: Long,
	val name: String,
)
fun LabelE.toRefI() = LabelRefI(id, name)

@Immutable data class TimeLabelI (
	val id: Long,
	val name: String,
	val parents: List<LabelRefI>,
	val children: List<LabelRefI>,
)


@Immutable data class PossibleTimePeriodI (
	/**If -1, this represents a time period that doesn't exist - a gap where we didn't record.
	 * In this case, start should be set to Long.MIN_VALUE*/
	val id: Long,
	val startMs: Long,
	val endMs: Long?,
	val samples: List<TimePeriodSampleI>,
	val description: String?,
) {
	val durationMs: Long? = if(endMs == null) null else endMs - startMs
}

fun TimePeriodE.toInfo(allLabelsSorted: List<LabelE>) =
	PossibleTimePeriodI(id, startMs, endMs, description = description, samples = samples.map { it.toInfo(allLabelsSorted) })


@Immutable data class TimePeriodSampleI (
	val id: Long,
	val weight: Long,
	val labels: List<LabelRefI>,
)

fun TimePeriodSampleE.toInfo(allLabelsSorted: List<LabelE>) =
	TimePeriodSampleI(id, weight, labelIds.map { labelLoader.sortedRequire(it, allLabelsSorted).toRefI() })