package imp

import android.view.Window

fun Window.addOrClearFlags(add: Boolean, flags: Int) {
	if(add)
		this.addFlags(flags)
	else
		this.clearFlags(flags)
}